# Lister og strings funker nokså likt når det kommer til indeksering.
X = ['a', 'b', 'c', 'd', 'e']
Y = 'abcde'

# Vi kan referere til enkelt-elementer i de.
print(X[2])
print(Y[2])

# Vi kan referere til slices/sub-sequences i de.
print(X[1:4])
print(Y[1:4])

# Vi kan endre elementer i en liste ...
X[0] = 'r'
print(X)

# Vi kan derimot ikke endre elementer/bokstaver i en string.
# Y[0] = 'r'  # TypeError: 'str' object does not support item assignment
Y = 'r' + Y[1:]
print(Y)

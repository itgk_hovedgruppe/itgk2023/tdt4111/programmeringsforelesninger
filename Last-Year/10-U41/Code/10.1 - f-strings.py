# Variabler (som er helt riktig, jeg lover).
navn = 'Omaba'
etternavn = 'Barrack'
alder = 59.1738465

# Sette sammen en f-string med verdiene, hvor vi runder alderen av til 0 år.
hilsen = f'Hei sann, {navn} {etternavn}! Nå er du {alder:.0f} år gammel! :D'
print(hilsen)


# Skrive ut inntekt, hvor vi forskyver tallene (setter kolonnebredden)
# med/til 10, sånn at tallene liner opp.
inntekt = 10_000
utgift = 7_500
print(f'{inntekt:10}')
print(f'{utgift:10}')


# Ved å sette kolonnebredde og antall desimaler, kan vi i praksis line opp
# tallene ved punktum/komma, slik vi gjerne vil.
inntekt = 12_345.6789
utgift = 7_500
print(f'{inntekt:10.2f}')
print(f'{utgift:10.2f}')

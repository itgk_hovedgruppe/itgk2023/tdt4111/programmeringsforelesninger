# Vi skal lese inn en CSV-fil, og lage teksten om til
# en tabell, slik vi gjorde i string-timen.

# Lese inn teksten i filen.
with open('data/data.csv', 'r') as fil:
    tekst = fil.read()


# Kopiert: Lage csv-tekst om til en Python-tabell.
def csv_til_tabell(tekst):
    # Splitte tekst inn i liste av rader, og fjern headers.
    rader = tekst.split('\n')
    rader = rader[1:]

    # Lag liste av rader til liste av lister.
    tabell = []
    for rad in rader:
        ny_rad = rad.split(', ')
        tabell.append(ny_rad)

    return tabell


# Kopiert: Printe ut tabellen på en pen måte.
def print_table(table):
    for row in table:
        for element in row:
            print(f'{element:25}', end=' ')
        print()


# Bruke innlest tekst og funksjoner til å
# lage og vise tabellen.
tabell = csv_til_tabell(tekst)
print_table(tabell)

def er_et_tall(x):
    '''Returnerer True hvis x er et tall.
    Returnerer False hvis x _ikke_ er et tall.'''
    try:
        # float(x) krasjer hvis x ikke er et tall.
        float(x)
        return True
    except ValueError:
        return False


print(er_et_tall(3.14))
print(er_et_tall('3.14'))
print(er_et_tall('3.1.4'))

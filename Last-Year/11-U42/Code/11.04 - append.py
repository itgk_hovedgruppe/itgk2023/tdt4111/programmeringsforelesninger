# Append er som write, men legger til på slutten.
# Fra for logging, hvis du skal legge til en linje per
# repetisjon av en eller annen oppgave.
with open('data/testfil.txt', 'a') as fil:
    fil.write('Dette er teksten i testfilen. ^_^\n')

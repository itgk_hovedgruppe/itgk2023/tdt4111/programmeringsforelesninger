# For å finne ut hvor programmet ditt ligger i koden.
# Denne skriver ut absolutt path. Altså filstien til filen
# fra bunnen/toppen av fil-treet/-hierarkiet.

# Bonus: Den skriver ut pathen til mappen programmet ditt
# kjører fra. Ikke hvor _denne_ filen ligger. Så hvis du
# importerer filen til et annet program, er det det andre
# programmet som filsti som skrives ut, ikke denne filen.
# Bare viktig når du lager egne moduler/filer for import.

import os

print(os.getcwd())

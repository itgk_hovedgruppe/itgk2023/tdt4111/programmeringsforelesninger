# Samme som i forrige oppgave, men litt mer:
# Nå skal vi oppdatere tabellen, for så å skrive
# tabellen ut til fil igjen. Da kan vi lese og skrive
# csv-filer som vi vil, ved å bare kopiere funksjonene.

with open('data/data.csv', 'r') as fil:
    tekst = fil.read()


def csv_til_tabell(tekst):
    # Splitte tekst inn i liste av rader, og fjern headers.
    rader = tekst.split('\n')
    rader = rader[1:]

    # Lag liste av rader til liste av lister.
    tabell = []
    for rad in rader:
        ny_rad = rad.split(', ')
        tabell.append(ny_rad)

    return tabell


def print_table(table):
    for row in table:
        for element in row:
            print(f'{element:25}', end=' ')
        print()


# Hente ut kolonnetitlene.
kolonnetitler = tekst.split('\n')[0]

tabell = csv_til_tabell(tekst)
print_table(tabell)

# Oppdatere tabellen
tabell[0][0] = 'Obama'
print_table(tabell)


# Skrive tabellen til en ny csv-fil.
with open('data/ny_data.csv', 'w') as fil:
    fil.write(f'{kolonnetitler}\n')
    
    for rad in tabell:
        #for verdi in rad:
        #    fil.write(f'{verdi}, ')
        
        fil.write(', '.join(rad))
        fil.write('\n')

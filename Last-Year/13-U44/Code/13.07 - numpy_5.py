# Vi kan lage numpy-arrays på flere måter.

import numpy as np


# Fra en Python-liste.
print(np.array([0, 1, 2, 3]))

# Likner med range, men kan ha float-steglengde.
print(np.arange(0, 1, 0.1))

# Likner arange, men vil ha antall punkter i stedet for steglengde.
print(np.linspace(0, 1, 10))

# Ofte kjekt å legge til 1 på antallet, så tallene blir penere. :)
print(np.linspace(0, 1, 11))

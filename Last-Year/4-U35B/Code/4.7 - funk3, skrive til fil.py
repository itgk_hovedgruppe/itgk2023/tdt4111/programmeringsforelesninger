# Definere funksjonen
def skriv_til_fil(filnavn, tekst):
    # Magi skjer her. Vi skal lære mer om det senere. :)
    with open(filnavn, 'w') as fil:
        fil.write(tekst)


# Kalle/kjøre funksjonen
skriv_til_fil('dagbok.txt', 'Kjære dagbok. Jeg skriver ikke dagbok. Før nå. :)')

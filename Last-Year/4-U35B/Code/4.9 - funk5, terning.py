# Magi. Vi importerer "random"-modulen, som kan brukes til å generere tilfeldige tall.
import random

# Definere funksjonen
def kast_terning():
    # Magi skjer her. Vi skal lære mer om det senere. :)
    terningkast = random.randint(1, 6)
    return terningkast

# Vi kaller/kjører funksjonen 3 ganger og lagrer kastene
kast1 = kast_terning()
kast2 = kast_terning()
kast3 = kast_terning()

# Skriver ut resultatet. Kjør koden flere ganger, så ser du at resultatene er forskjellige hver gang.
print(kast1)
print(kast2)
print(kast3)

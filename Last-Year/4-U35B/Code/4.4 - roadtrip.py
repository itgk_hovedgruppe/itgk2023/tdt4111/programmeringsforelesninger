# Du og noen venner har vært på road trip, Sverige-tur, eller noe sånt.
# Dere vet hvor mange liter dere har brukt, hvor dyrt det er per liter, og hvor mange dere er.
# Programmet under kan regne ut hvor mye hver person burde betale.


# Endre disse variablene
liter_med_bensin = 25
bensinpris = 19.30
personer = 4

# Utregninger
kroner = liter_med_bensin * bensinpris
per_person = kroner / personer

# Skrive ut resultat
print(f'Totaltprisen ble {kroner}, som blir {per_person} per person.')

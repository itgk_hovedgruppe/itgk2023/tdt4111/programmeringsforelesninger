# Dicts

# Hvordan lagre masse info om en person?

# Bruke flere variabler
# Tydelig, men blir mange variabler.
navn = 'Paul'
alder = 29
penger = 100_000
utdanning = 'datavitenskap'
stilling = 'universitetslektor'

# Lagre alt i en liste
# Kompakt, men vanskelig å se at indeks 3 betyr utdanning.
# navn, alder, penger, utdanning, stilling
meg = ['Paul', 29, 100_000, 'datavitenskap', 'universitetslektor']
print(meg[3])

# Lagre i en dict
# Enkelt å se hva som refereres til. 'utdanning' er utdanningen. Obviously.
meg2 = {
    'navn': 'Paul',
    'alder': 29,
    'penger': 100_000,
    'utdanning': 'datavitenskap',
    'stilling': 'universitetslektor'
}
print(meg2['utdanning'])

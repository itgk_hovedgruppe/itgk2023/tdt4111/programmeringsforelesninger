# Oppdatert litt fra forelesning, med noe enklere eksempel.
# Ofte bruker vi en datatype inni en annen datatype. F.eks. en liste med dicts.

# Eksempel: Vi har et sett med filer vi vil gi nye navn.
# Vi kan da lage to lister:
#   * Gamle navn
#   * Nye navn
# I dette tilfellet skal de bare oversettes fra norsk til engelsk.

# gamle = ['Rapport.pdf', 'Kode.py', 'Oppgaver.docx']
# nye = ['Report.pdf'm 'Code.py', 'Problems.docx']

# Dette funker fint, men det hadde vært greiere å kunne
# koble 'Kode.py' og 'Code.py' mer sammen i datastrukturen vår.
# Det kan vi gjøre med en liste av tuples, hvor hver tuple har
# gammel og ny verdi, og listen inkluderer alle filnavnendringene.


# Liste med 3 tuples, hvor hver tuple har 2 verdier.
files_to_rename = [
    ('Rapport.pdf', 'Report.pdf'),
    ('Kode.py', 'Code.py'),
    ('Oppgaver.docx', 'Problems.docx')
]

# Gå gjennom listen, en tuple av gangen.
for fil_par in files_to_rename:
    print(fil_par)


# Bedre eksempel: Lister i dicts.
# Hvis vi vil lagre brukerne til et system, er det ofte greit å hente
# informasjon basert på brukernavnet. Vi lager en dict, hvor hver key er en
# mailadresse, og hver value er en liste med verdier om brukeren.
brukere = {
    'abc@gmail.com': ['Paul', 29, 100_000],
    'erna@solberg.no': ['Erna', 60, 5_000_000]
}
# Vi kan også huske fra eksempelet vi introduserte dicts med, at det kanskje
# passer bedre med en dict her, enn en liste. Da får vi dicts i dicts. :D

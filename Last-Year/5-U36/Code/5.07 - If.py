# Skriver bare inn om det er godkjent eller ikke.
# Da slipper vi å kopiere krav og kravsjekk før denne kodesnutten.
godkjent = True

# Om laptopen er godkjent, kjøper vi den. Om ikke, kjøper vi den ikke.
if godkjent:
    print('Kjøp den!')
else:
    print('Ikke kjøp den!')

# Vi kan gjøre "if not godkjent" i stedet for "else", men else er mer praktisk her.
if not godkjent:
    print('Ikke kjøp den!')

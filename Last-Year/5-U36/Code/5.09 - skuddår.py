# Her fyller vi bare inn om det er skuddår eller ikke.
# Neste eksempel viser hvordan vi kan finne denne mer automatisk.
skuddår_i_år = True

# Hvis det er skuddår, så er det 29 dager i februar.
if skuddår_i_år:
    antall_dager_i_februar = 29

# Ellers er det 28.
else:
    antall_dager_i_februar = 28

# Skriver ut som en fin beskjed.
print(f'Det er {antall_dager_i_februar} dager i februar i år.')

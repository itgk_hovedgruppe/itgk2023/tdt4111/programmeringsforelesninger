# Vi vil kjøpe en laptop, og har følgende krav:
# * Den må koste mer enn 8000 (sånn at den er bra nok)
# * Den må koste mindre enn 15000 (sånn at vi har råd til den)
# Dette kan skrives slik:
# 8000 < pris < 15000
# Eller slik:
# pris > 8000 og pris < 15000

# Bruker fyller inn verdier
pris = 17_000

# Sjekke delkravene
ikke_for_lav = pris > 8000
ikke_for_høy = pris < 15000
# print(ikke_for_lav, ikke_for_høy)

# Finne og skrive ut resultat av full kravsjekk
akseptabel_pris = (ikke_for_lav and ikke_for_høy)
print(akseptabel_pris)

# Alternativ (mer direkte) variant, uten bruk av "and"
akseptabel_pris = (8000 < pris < 15000)
print(akseptabel_pris)

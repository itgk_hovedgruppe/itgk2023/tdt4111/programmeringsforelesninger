import os


# Finne ut hvor i filsystemet ditt programmet ditt ligger.
print(os.getcwd())

# Skrive ut filene i samme mappe som programmet ditt.
print(os.listdir())

# Skrive ut filene i undermappen 'flere_filer' i samme mappe
# som programmet ditt.
filer = os.listdir('flere_filer')
print(filer)

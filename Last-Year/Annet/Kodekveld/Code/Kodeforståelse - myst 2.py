def myst(tall1,tall2,nr):
    for i in range(2,nr):
        nytt = tall1 + tall2
        tall1 = tall2
        tall2 = nytt
    return nytt

print(myst(0, 1, 7))

'''
i   nytt    tall1   tall2
-   -       0       1
2   1       1       1
3   2       1       2
4   3       2       3
5   5       3       5
6   8       5       8

n = [3, 7]
'''

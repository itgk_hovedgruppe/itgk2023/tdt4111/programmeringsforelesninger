## Agenda
1. Oversikt av resten av semesteret
2. Plan for dagen
    - Update dicts
    - Numpy ++
    - Eksamen?
    - Filer (mer)
3. Trondheimtur: Sannsynligvis uke 46
4. Kodekveld-spørsmål til Gjøvik etter forelesning/stream

## Plan ut semesteret
- Repetisjon, filer
- Repetisjon, exceptions
- Numpy og Matplotlib
- Fil-string-analyse-oppgave
- Mer om iterering
- Eksamensløsing

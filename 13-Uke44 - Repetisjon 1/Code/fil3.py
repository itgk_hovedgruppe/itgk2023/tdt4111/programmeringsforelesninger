# Se i sammenheng: fil1, fil2, fil3

# Lage en liste med verdier.
talliste = ['0', '1', '2', '3', '4', '5']

# Lage en tom oppsamlingsvariabel (summ).
summ = ''

# Gå gjennom en og en verdi i listen.
for tall in talliste:
    # Dytt tall inn i oppsamlingsvariabelen.
    summ += tall
    # summ += '|' + tall + '|\n'

print(summ)

'''
Alle tre (fil1, fil2, fil3) har ca. samme struktur. Det er det jeg kaller en
"oppsamlingsløkke". Altså en løkke som "samler opp" alt i en liste, kanskje
gjør noe med enkeltverdiene, og dytter det inn i en oppsamlingsvariabel. F.eks.
en string, en int eller en liste.
'''

## Skrive fil

# Se i sammenheng: fil10 og fil11


# Alternativ 1 for å åpne fil.
with open('filnavn.txt', 'w') as fil:
    fil.write('heihei')


# Alternativ 2 for å åpne fil.
fil = open('filnavn.txt', 'w')  # Åpne fil i write-mode.
fil.write('heihei')  # Skrive tekst til filen.
fil.close()  # Lukke filen når ferdig.

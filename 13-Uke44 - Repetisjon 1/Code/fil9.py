# Nesten riktig løsning på økonomisk analyse deloppgave a) (tror jeg)
# På bare en linje! :D

# Importere pandas
import pandas as pd


# - pd.read_csv(filnavn) returnerer en dataframe - basically et Excel-ark - med
#   verdiene fra csv-filen (df står for dataframe)
# - header=None, names=['Selskap', 'Inntekt'] fordeller pandas at csv-filen
#   ikke har med headers (overskrifter), og så setter vi de heller manuely.
# - to_dict() gir tilbake en dictionary fra dataframe-filen. Argumentet
#   forteller hva slags form vi vil ha. 'records' er den nærmeste.
d = pd.read_csv('revenue2020.txt', header=None, names=['Selskap', 'Inntekt']).to_dict('records')

# Hvis du vil løse oppgaven med pandas, kan du se et litt mer riktig eksempel
# i forelesningsnotatene mine, i GitLab (link fra Blackboard).

# Skrive ut dicten.
print(d)

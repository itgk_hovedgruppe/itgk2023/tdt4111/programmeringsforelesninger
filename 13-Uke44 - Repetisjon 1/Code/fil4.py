# Se i sammenheng: fil4, fil5, fil6

# Lage en tabell.
tabell = [
    #0  1
    [1, 2], # 0
    [3, 4], # 1
    [5, 6]  # 2
]


# Lage oppsamlingstabell.
ny_tabell = []

# For hver rad i tabell...
for rad in tabell:
    # Lage oppsamlingsrad.
    ny_rad = []
    # For hvert tall i rad...
    for tall in rad:
        # Legge til tall i oppsamlingsrad. (Og gange med 1000.)
        ny_rad.append(tall * 1000)
    # (For hver rad) legge til oppsamlingsrad i oppsamlingstabell.
    ny_tabell.append(ny_rad)


print(ny_tabell)

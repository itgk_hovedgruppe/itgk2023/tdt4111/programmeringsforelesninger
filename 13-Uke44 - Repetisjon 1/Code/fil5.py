# Se i sammenheng: fil4, fil5, fil6

# Lage en tabell.
tabell = [
    #0  1
    [1, 2], # 0
    [3, 4], # 1
    [5, 6]  # 2
]


# Gå gjennom indekser (i) for hver rad.
for i in range(len(tabell)):  # rad
    # Gå gjennom indekser (j) for hver kolonne.
    for j in range(len(tabell[i])):  # kolonner
        # Gange hvert element i tabellen med 1000.
        tabell[i][j] = tabell[i][j] * 1000
        # tabell[i][j] *= 1000  # Mer kompakt metode.


print(tabell)

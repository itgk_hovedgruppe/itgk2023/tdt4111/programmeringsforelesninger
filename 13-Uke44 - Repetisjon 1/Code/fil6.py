# Se i sammenheng: fil4, fil5, fil6

# Import numpy, så vi kan bruke numpy-arrays.
import numpy as np

# Lage en tabell.
tabell = [
    #0  1
    [1, 2], # 0
    [3, 4], # 1
    [5, 6]  # 2
]

# Lage tabellen til et 2D numpy-array.
array = np.array(tabell)

# Gange arrayet (hvert element i arrayet) med 1000.
array *= 1000

# Ev. alt dette på en enkeltlinje.
array = np.array(tabell) * 1000

print(array)

'''
NB! Numpy-array er ikke en liste/tabell. Den har mange likheter, og kan gjøre
mye av det samme, men funker ikke alltid helt likt. Kjempebra for matte.
'''

## Lese fil

# Se i sammenheng: fil10 og fil11

# Delvis kopiert fra fil10.
fil = open('filnavn.txt', 'r')  # Åpne fil i read-mode.
tekst = fil.read()  # Lese fra filen (og lagre tekst i variabel).
fil.close()  # Lukke filen når ferdig.

# Skrive ut resultat for å se at det virket.
print(tekst)

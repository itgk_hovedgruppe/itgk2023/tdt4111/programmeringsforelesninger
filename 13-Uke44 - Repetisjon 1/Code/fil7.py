import numpy as np


# Samme tabell som i fil4-6.
tabell = [
    #0  1
    [1, 2], # 0
    [3, 4], # 1
    [5, 6]  # 2
]

# x-verdier er tabellen.
x = np.array(tabell)

# y-verdier er tabellen, men hvor hvert element er kjørt gjennom matten under.
y = 3*x**2 - 4*x + 7
# y = f(x)  # Eller med en faktisk funksjon.

print(y)

# Importere numpy, for matte og arrays - og matplotlib, for grafer.
import numpy as np
import matplotlib.pyplot as plt


# Se Excel-ark (Manuell-plotting.xlsx) i sammenheng.

# Vanlig Python har range. Numpy har sin egen: arange. Numpy har også linspace.
# range(start, stop, steglengde)
x = np.arange(0, 5, 0.01)  # start, stop, steglengde.
x = np.linspace(0, 5, 500)  # start, stop, antall-elementer.

# Lage en mattefunksjon.
def f(x):
    return x**2

# Lage y-verdier. y ved brukt av funksjonen f, y2 ved å bare gange direkte.
y = f(x)
y2 = 2*x**2 - 3*x

# Plotte x-verdier mot y-verdier og y2-verdier.
plt.plot(x, y)
plt.plot(x, y2)
plt.show()  # Åpne plot/graf.

'''
Søk på "matplotlib examples" på google o.l. for hundrevis av nyttige og
kopierbare eksempler på alle mulige slags grafer.

Bruk funksjoner som:
- plt.xlabel og plt.ylabel for å legge til labels
- plt.savefig for å lagre som bildefil
- plt.legend for å legge til beskrivende tekst i grafen
- plt.title o.l. for å legge til tittel

og maaaaange mer.

Legg til ekstraargumenter i plt.plot for å gjøre flere småting. F.eks. gir
plt.plot(x, y, '--r')
tilbake en graf med x og y, hvor linjen er siplet (--) og rød (r).
'''

# Programmering 4 (v1)

ansatte = [
    {
        'fornavn': 'George',
        'mellomnavn': 'W.',
        'etternavn': 'Bush',
        'alder': 70,
        'land': 'US',
        'email': 'noe@outlook.no'
    },
    {
        'fornavn': 'Erna',
        'mellomnavn': '',
        'etternavn': 'Solberg',
        'alder': 30,
        'land': 'NO',
        'email': 'noe@gmail.com'
    }
]


def get_matches(ansatte):
    aktuelle = []

    for ansatt in ansatte:
        # if ansatt['land'] in ['NO', 'US']:
        if ansatt['land'] == 'NO' or ansatt['land'] == 'US':
            if 18 <= ansatt['alder'] <= 35:
                if ansatt['email'].endswith('@gmail.com'):
                    if len(ansatt['mellomnavn']) == 0:
                        aktuelle.append(ansatt)

    return aktuelle


print(get_matches(ansatte))

'''
1. Er fra Norge eller USA. ('NO' eller 'US'.)
2. Er fra og med 18 til og med 35 år gamle.
3. Bruker gmail.com til mail.
4. Ikke har et (registrert) mellomnavn.
'''

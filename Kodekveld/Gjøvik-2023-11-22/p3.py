# Programmering 3

def find_matches(A, B, C):
    AB_C = []
    for a in A:
        if a in B and a not in C:
            AB_C.append(a)

    return AB_C


#                      A         B       C
print(find_matches([1, 2, 3], [1, 2], [2, 3]))  # {1}
print(find_matches([1, 2, 3], [0, 2], [2, 3]))  # set()

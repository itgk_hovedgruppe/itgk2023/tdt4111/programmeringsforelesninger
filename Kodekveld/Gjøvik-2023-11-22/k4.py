# Kodeforståelse 4

def lag_hilsen(navneliste):
    # if len(navneliste) == 0:
    #     return 'Hilsen alle sammen!'
    personer = navneliste[0] # Legg til første navn.
    for navn in navneliste[1:-1]: # Iterer over alle utenom første og siste.
        personer += ', ' + navn # Legg til komma og neste navn.

    personer += ' og ' + navneliste[-1] # Legg til " og {siste navn}".
    hilsen = 'Hilsen ' + personer + '!'
    return hilsen


print(lag_hilsen(['Bill', 'Obama', 'Erna']))
print(lag_hilsen([]))

'''
Velg ett alternativ:
- navneliste[-1] er ikke lov, da alle elementene i listen allerede er iterert over.
- Vi kan ikke legge en liste med indekser sammen med en string. "String + liste" er ikke lov.
- Funksjonen krasjer hvis navnelisten er tom.
- Slicingen ([1:-1]) bruker negativ indeks, som ikke er lov.
'''

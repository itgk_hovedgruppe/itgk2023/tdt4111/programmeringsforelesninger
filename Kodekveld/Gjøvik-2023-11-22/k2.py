# Kodeforståelse 2

# Her har jeg endret koden for å vise hva vi forventer hvor. Originalkoden
# finnes på eksamen i Blackboard.

def funksjon(a, b, c):
    b['2'] = 3
    return b

a = '123'[1]
b = {
    'r': 1,
    2: 'p',
    '2': 3
}
c = 3
y = funksjon(a, b, c)
print(y)

'''
Velg ett alternativ:

- {'r': 1, 2: 'p', '2': '3'}
- {'r': 1, 2: 3}
- {'r': 1, 2: 'p', '2': 3}
- {'r': 1, 2: 'p3'}
'''

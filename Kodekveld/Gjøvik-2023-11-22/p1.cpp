/*
 * Programmering 1 (i C++)
 * 
 * Feel free to ignore this entire file. Den er så langt utenfor pensum som det
 * går an.
 * 
 * Jeg gjorde ikke denne ferdig under Kodekvelden, men her er en omtrentlig
 * riktig versjon av Programmering 1 i C++.
 */


// Vi må importere iostream for å skrive ut til skjerm, og vector for vector/lister.
#include <iostream>
#include <vector>


// Funksjonen vi skulle lage.
std::vector<std::vector<int> > add (std::vector<std::vector<int> > tabell1,
                                   std::vector<std::vector<int> > tabell2) {
    for (int i = 0; i < tabell1.size(); i++) {
        for (int j = 0; j < tabell1[i].size(); j++) {
            tabell1[i][j] += tabell2[i][j];
        }
    }
}


// En main-funksjon som brukes for å kjøre og teste funksjonen vår.
int main() {
    // Funker bare i C++11 og nyere.
    std::vector<std::vector<int> > tabell1 = {{17, 8, 12}, {9, 3, 18}};
    std::vector<std::vector<int> > tabell2 = {{7, 8, 9}, {10, 11, 12}};

    std::vector<std::vector<int> > tabell3 = add(tabell1, tabell2);

    // Utskrift av tabell3.
    for (int i = 0; i < tabell3.size(); i++) {
        for (int j = 0; j < tabell3[i].size(); j++) {
            std::cout << tabell3[i][j] << " ";
        }
        std::cout << std::endl;
    }
    return 1;
}

// Kompiler med følgende kommando
// g++ -std=c++11 p1.cpp -o p1

// Nei da, den krasjet haha. Orker ikke å debugge. Ikke burk C++. Ikke kos. :(

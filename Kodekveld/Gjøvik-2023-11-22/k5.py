# Kodeforståelse 5

#    01234
r = 'agurk'
p = list(r)
p.sort()
p[2] += p[3]
t = ''.join(p)
print(t)

'agkrru'

'''
0 a
1 g
2 kr
3 r
4 u
'''

'''
hei
'hei'
"hei"
`hei`
'''

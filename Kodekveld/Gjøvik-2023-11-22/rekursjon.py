'''
Rekursjon
Pensum i TDT4109 og sikkert TDT4110. Ikke i TDT4111. Så feel free to ignore.

Rekursjon er når en funksjon kaller seg selv inni seg selv.
Basically inception.

For fakultet/factorial-funksjonen kan vi bruke rekursjon.
'''

def fact(n):
    if n < 2:
        return n
    else:
        return fact(n - 1) * n


print(fact(4))


# n! = n * (n-1) * (n-2) * ... 3 * 2 * 1

# 4! = 4 * 3 * 2 * 1
# 4! = 4 * 3!
# 4! = 4 * 3 * 2!
# 4! = 4 * 3 * 2 * 1!
# 4! = 4 * 3 * 2 * 1
# 4! = 24

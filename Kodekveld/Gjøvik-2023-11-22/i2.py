# Fyll inn 2

def funksjon(a, b):
    c = ''
    for A in a:
        if A in b:
            c += A
    return c


print(funksjon('plante', 'agurkbriller'))

'''
To måter å løse denne på her:
1. Gå gjennom en og en iterasjon av loopen, track hva verdiene er per iterasjon
    og finn ut hva c ender opp med å være.
2. Tolk koden og se om den gjør noe nyttig/forståelig.

Løsning 2:
I dette tilfellet er nok løsning 2 best her. Vi kan se - forhåpentligvis - at
c vil få alle bokstavene/verdiene som er i både a og b. Rekkefølgen blir den
samme som i a. Bokstavene `lae` er i a og b. Dermed er det svaret.

Løsning 1:
Sjekk hva resultatet blir for hver iterasjon av loopen. Jeg tracker if-en her:

p   False
l   True
a   True
n   False
t   False
e   True

Dermed kan vi se at `c` burde få bokstavene l, a og e, da disse gir if True.
Svaret blir altså `lae`.

c = 'lae'
'''

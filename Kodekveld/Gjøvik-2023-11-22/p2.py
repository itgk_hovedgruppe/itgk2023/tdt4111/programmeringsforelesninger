# Programmering 2

def find_matches(A, B, C):
    A = set(A)
    B = set(B)
    C = set(C)

    AB = A.intersection(B)
    AB_C = AB.difference(C)

    # AB_C = A.intersection(B).difference(C)
    return AB_C


#                      A         B       C
print(find_matches([1, 2, 3], [1, 2], [2, 3]))  # {1}
print(find_matches([1, 2, 3], [0, 2], [2, 3]))  # set()

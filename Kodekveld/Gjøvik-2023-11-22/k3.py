# Kodeforståelse 3

import modules.strukturmodul as str
import modules.forskningsmodul as for
import modules.databasemodul as dat

def funksjon():
    while True:
        for i in range(str.hent_liste()):
            dat.lagre(i)
            if dat.full():
                return

funksjon()

'''
- vi tar ikke i mot noe fra funksjonen. F.eks. `verdi = funksjon()`.
- forskningsmodulen kan ikke forkortes til `for`.
- while True gjør at den aldri vil bli ferdig.
- funksjonen har glemt å returnere en verdi.
'''


'''
Når jeg åpenr koden i VS Code, får jeg med en gang en rød strek under feilen.
Vi kan ikke importere noe `... as for`, fordi `for` er et reservert ord i
Python.

Teknisk sett _kan_ vi importere noe `... as str`, selv om `str` er et nøkkelord
i Python. Men det er ikke god kodepraksis.
'''

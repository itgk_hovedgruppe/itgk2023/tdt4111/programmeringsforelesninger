# Programmering 1 (uten numpy)

def add(tabell1, tabell2):
    # ny_tabell = [
    #     [0, 0, 0],
    #     [0, 0, 0]
    # ]

    for i in range(len(tabell1)):  # rad-indeks
        for j in range(len(tabell1[i])):  # kolonne-indeks
            tabell1[i][j] += tabell2[i][j]
            # ny_tabell[i][j] = tabell1[i][j] + tabell2[i][j]

    return tabell1


tabell1 = [
    # 0  1   2
    [17, 8, 12],  # 0
    [ 9, 3, 18]   # 1
]

tabell2 = [
    [7, 8, 9],
    [10, 11, 12]
]

print(add(tabell1, tabell2))


[
    [24, 16, 21],
    [19, 14, 30]
]

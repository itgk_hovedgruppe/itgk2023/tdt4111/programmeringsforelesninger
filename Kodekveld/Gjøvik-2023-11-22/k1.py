# Kodeforståelse 1

def funksjon1(fname, x):
    with open(fname, 'w') as f:
        f.write(f'{x}')

def funksjon2(fname):
    with open(fname, 'r') as f:
        y = f.read()
    return y

verdier = [1, 2, 3]
funksjon1('testfil.txt', verdier)
nye_verdier = funksjon2('testfil.txt')

# print(type(nye_verdier))

'''
- list of ints
- list of strings
- string of lists
- string
'''


'''
Løsning:

Her ville jeg sett på koden litt i motsatt rekkefølgen.
- `nye_verdier` blir returnert fra `funksjon2`.
- `funksjon2` leser fra en fil, og returnerer det den leser.
- Vi vet at vanlig fil-lesing (with open og f = open) _alltid_ gir en string.
- Dermed vet vi at `nye_verdier` er en string. Svaret er string.
'''

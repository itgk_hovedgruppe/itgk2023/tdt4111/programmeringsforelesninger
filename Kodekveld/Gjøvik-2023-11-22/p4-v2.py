# Programmering 4 (v2)

ansatte = [
    {
        'fornavn': 'George',
        'mellomnavn': 'W.',
        'etternavn': 'Bush',
        'alder': 70,
        'land': 'US',
        'email': 'noe@outlook.no'
    },
    {
        'fornavn': 'Erna',
        'mellomnavn': '',
        'etternavn': 'Solberg',
        'alder': 30,
        'land': 'NO',
        'email': 'noe@gmail.com'
    }
]


def get_matches(ansatte):
    aktuelle = []

    for ansatt in ansatte:
        if ansatt['land'] != 'NO' and ansatt['land'] != 'US':
            continue

        if not (18 <= ansatt['alder'] <= 35):
            continue

        if not ansatt['email'].endswith('@gmail.com'):
            continue

        if len(ansatt['mellomnavn']) != 0:
            continue

        aktuelle.append(ansatt)

    return aktuelle


print(get_matches(ansatte))

'''
1. Er fra Norge eller USA. ('NO' eller 'US'.)
2. Er fra og med 18 til og med 35 år gamle.
3. Bruker gmail.com til mail.
4. Ikke har et (registrert) mellomnavn.
'''

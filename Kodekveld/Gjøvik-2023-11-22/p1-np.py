# Programmering 1 (med numpy)

import numpy as np


def add(tabell1, tabell2):
    # return np.array(tabell1) + np.array(tabell2)
    array1 = np.array(tabell1)
    array2 = np.array(tabell2)

    nytt_array = array1 + array2
    return nytt_array


tabell1 = [
    # 0  1   2
    [17, 8, 12],  # 0
    [ 9, 3, 18]   # 1
]

tabell2 = [
    [7, 8, 9],
    [10, 11, 12]
]

print(add(tabell1, tabell2))

[
    [24, 16, 21],
    [19, 14, 30]
]

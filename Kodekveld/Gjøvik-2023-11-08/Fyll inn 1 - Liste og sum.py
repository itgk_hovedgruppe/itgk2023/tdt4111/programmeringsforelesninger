#0  1  2  3  4
[1, 2, 3, 4, 5]  # lage
[5, 4, 3, 2, 1]  # reversere rekkefølgen
[5, 4, 7, 2, 1]  # oppdatere indeks 2 til 7
[5, 4, 7, 2, 1, 6]  # legge til 6 på sluten
# 5+4+7+2+1+6=25  # regne ut svaret

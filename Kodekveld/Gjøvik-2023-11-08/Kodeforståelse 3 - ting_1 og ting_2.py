# Kode
def funksjon(ting_1, ting_2):
    ting_1[ting_2] = str(0) + ting_2
    return ting_1


print(funksjon({'x': 123, 'y': 456, 'z': 789}, 'y'))


# Tankegang
def funksjon(ting_1, 'y'):
    ting_1['y'] = '0y'
    return ting_1


print(funksjon({'x': 123, 'y': '0y', 'z': 789}, 'y'))

import numpy as np


utgifter = [
    
# lønn utstyr annet
    
[120,  1007,  320],  # selskap 1
    
[290,   500,  300],  # selskap 2
    [ 50,   500,   70]   # selskap 3
]

# utgifter *= 1000

def til_kroner_1(utgifter):
    for i in range(3):
        for j in range(3):
            utgifter[i][j] *= 1000
    return utgifter


tabell = til_kroner_1(utgifter)

liste = [1, 2, 3]

ny_liste = []
for tall in liste:
    ny_liste.append(tall * 1000)


def til_kroner_2(utgifter):
    ny_tabell = []
    for rad in utgifter:
        ny_rad = []
        for tall in rad:
            ny_rad.append(tall * 1000)
        ny_tabell.append(ny_rad)
    return ny_tabell


def til_kroner_3(utgifter):
    array = np.array(utgifter)
    array *= 1000
    return array


def til_kroner_4(utgifter):
    return np.array(utgifter) * 1000

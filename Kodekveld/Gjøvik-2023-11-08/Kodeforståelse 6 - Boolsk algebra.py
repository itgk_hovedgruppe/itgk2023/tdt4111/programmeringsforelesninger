# a
a = (True and False) or (True and True)
a = False or True
a = True

# b
b = (3 == 4)
b = False

# c
c = not(b or a)
c = not(True or False)
c = not(True)
c = False

# d
d = ('x' in 'xyz')
d = True

# Resultat
a = True
b = False
c = False
d = True

# Kodekveld 1 i Gjøvik
- Dato: 2023-11-08
- Tid: 16:30 - 20:30

Opptak finnes på Blackboard.

På kodekvelden gikk vi gjennom mye - men ikke alt - av en tidligere eksamen: Ordinær eksamen nr. 1 i 2022. Denne finnes med og uten løsningsforslag på Blackboard.

Filene er ikke alltid godt kommentert, da de hører til en forelesning. Se ev. den, hvis du vil ha bedre forklaring.

Se også filen `Tabeller.pdf` eller `Tabeller.xlsx` (samme fil, forskjellig format) for Excel-eksempelet mitt når det kom til tabeller og indekser.

Python cheat-sheet er også lagt til. Det vil nok også være tilgjengelig på eksamen, sammen med [Python-docs](https://docs.python.org/3/) (hvorav `Tutorial`-seksjonen er mest aktuell) og dokumentasjon for NumPy og Matplotlib.

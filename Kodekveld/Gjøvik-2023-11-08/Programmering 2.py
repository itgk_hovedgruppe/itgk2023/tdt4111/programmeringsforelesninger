def les_fra_fil(filnavn):
    with open(filnavn, 'r') as f:
        tekst = f.read()
    
    '''
    f = open(filnavn, 'r')
    tekst = f.read()
    f.close()
    '''

    linjer = tekst.split('\n')
    return linjer


def tell_prosent_00(linjer):
    antall = 0
    for linje in linjer:
        if linje.endswith('.00'):
            antall += 1

    prosent = antall / len(linjer) * 100
    return prosent


linjer = les_fra_fil('data.txt')
print(linjer)

prosent = tell_prosent_00(linjer)
print(f'{prosent}%')

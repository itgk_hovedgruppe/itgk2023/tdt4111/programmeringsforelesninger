members = {
    'Anne': [35, 'Norge', 'CEO'],
    
'Bjarne': [55, 'Norge', 'sekretær'],
    
'Carl': [45, 'USA', 'software-utvikler'],
    
'Diana': [28, 'UK', 'vaktmester']


}

countries_fasit = {
    'Norge': 2,
    'USA': 1,
    'UK': 1

}


def get_countries(members):
    countries = {}

    for value in members.values():
        country = value[1]

        if country in countries:
            countries[country] += 1
        else:
            countries[country] = 1

    return countries


print(get_countries(members))

## While-løkker

# Lager en variabel.
x = 1

# Så lenge x < 3:
while x < 3:
    # Øk x med 1.
    x += 1  # x = x + 1

# Skriv ut resulterende x.
print(x)

'''
Klesbutikk på nett. Vi vil søke etter varer.
'''

# Om meg
min_størrelse = 38
max_størrelse = 42
max_pris = 800
er_dame = False


def akseptabelt_plagg(size, price, for_ladies):
    if price > max_pris:
        return False
    
    if er_dame != for_ladies:
        return False
    
    # Her gjorde jeg feil i forelesningen før pausen, men rettet opp etterpå.
    if not (min_størrelse <= size <= max_størrelse):
        # if min_størrelse > size or size > max_størrelse:
        return False

    return True


# Om plagget + kjøre funksjonen
print(akseptabelt_plagg(40, 600, False))

'''
NB! Dette er en alternativ måte å skrive kommentarer på i Python.


Krav for True / testen
- positivt: x >= 0
- partall: x % 2 == 0
- 10 eller større: x >= 10
- 100 eller mindre: x <= 100

10 <= x <= 100

Her splitter vi testene inn i fire separate negative/inverterte tester.
I stedet for å sjekke om alle testene stemmer, sjekker vi om en og en _ikke_
stemmer. Hvis ikke, er vi ferdig. Returner False.
'''

def godkjent(x):
    # (x >= 0) and (x % 2 == 0) and (x >= 10) and (x <= 100)
    
    if x < 0:
        return False
    
    if x % 2 != 0:
        return False
    
    if x < 10:
        return False
    
    if x > 100:
        return False
    
    return True


print(godkjent(18))

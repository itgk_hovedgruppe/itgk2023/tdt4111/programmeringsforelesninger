# Samme som forrige, men med print(x), og en kommentar.

x = 1


while x < 3:
    print(x)
    x += 1
    # Hvis du glemmer x += 1, vil whilen fortsette for alltid. Trykk på
    # - stopp (Thonny)
    # - Restart kernel (Jupyter)
    # for å avbryte den.

print(x)

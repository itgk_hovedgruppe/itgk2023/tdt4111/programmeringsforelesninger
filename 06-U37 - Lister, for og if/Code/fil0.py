# Spørsmål fra student: Hvorfor funker ikke "svar == 'j' or 'J'?"
# Kort sagt: "or" splitter det i to påstander: "svar == 'j'" og "'J'".
# 'J' er ikke en påstand, men det er en ikke-null/tom-verdi, som gjør at den
# teller som "True" i koden. Altså er 'J' alltid True, og if-en vil alltid
# kjøre. Riktig løsning finnes under.

svar = 'j'

if svar == 'j' or svar == 'J':
    print('jaa')
else:
    print('Neh >:(')


# Alternativ variant som bruker lister:
# if svar in ['j', 'J']: ...

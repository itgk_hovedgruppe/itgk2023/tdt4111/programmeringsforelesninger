# Lage liste.
navneliste = ['Paul', 'Ada', 'Obama', 'Alan']

# Skrive ut navneliste på indeks i, hvor i er 0, 1, 2, 3
for i in range(len(navneliste)):
    print(navneliste[i])

# Skrive ut navneliste uten å bruke indekser.
for navn in navneliste:
    print(navn)

# Versjon to er mer praktisk i de mange tilfeller, men i noen tilfeller kan en
# ikke bruke det, og må ty til indekser.

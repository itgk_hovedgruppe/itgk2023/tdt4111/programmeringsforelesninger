## For-løkker

# Lager en liste med tall.
navneliste = ['Paul', 'Ada', 'Obama', 'Alan']

# Henter ut ett og ett navn i navnelisten, kaller det "navn", og printer det.
for navn in navneliste:
    print(navn)

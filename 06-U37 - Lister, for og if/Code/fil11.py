## Oppgave: Hvor mange ganger må x dobles for å bli større enn 10 000?

# Lager x og en tellevariabel.
x = 1
teller = 0

# Så lenge x < 10_000:
while x < 10_000:
    # Doble x.
    x *= 2  # x = x * 2

    # Øk teller med 1.
    teller += 1

# Skriv ut teller, altså antallet doblinger vi gjorde før whilen avsluttet.
print(teller)

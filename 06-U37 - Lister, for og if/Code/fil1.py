# Funksjon som sjekker om tall er positivt eller negativt.
tall = -2


def er_positiv(tall):
    # hvis tall er positivt: Returner True
    # hvis ikke: Returner False
    
    if tall >= 0:
        return True
    else:
        return False


print(er_positiv(tall))

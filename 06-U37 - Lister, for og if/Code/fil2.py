'''
NB! Dette er en alternativ måte å skrive kommentarer på i Python.


Krav for True / testen
- positivt: x >= 0
- partall: x % 2 == 0
- 10 eller større: x >= 10
- 100 eller mindre: x <= 100

10 <= x <= 100
'''

def godkjent(x):
    if (x >= 0) and (x % 2 == 0) and (x >= 10) and (x <= 100):
        return True
    else:
        return False


print(godkjent(18))

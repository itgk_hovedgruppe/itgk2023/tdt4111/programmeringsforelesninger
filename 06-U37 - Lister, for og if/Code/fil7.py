# range(n) gir oss 0, 1, 2, ..., n-1
# Veldig bra til å bruke for å finne indekser.

for i in range(10):
    print(i)

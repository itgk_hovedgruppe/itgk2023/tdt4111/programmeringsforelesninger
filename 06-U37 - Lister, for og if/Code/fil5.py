## Lister

# Vi lager en liste.
talliste = [1, 2, 3, 4, 5]
print(talliste)

# Vi lager en annen liste.
navneliste = ['Paul', 'Ada', 'Obama', 'Alan']
print(navneliste)

# Vi skriver ut hvert element i listen, basert på hver verdi sin indeks.
print(navneliste[0])
print(navneliste[1])
print(navneliste[2])
print(navneliste[3])

# Vi oppdaterer verdien på indeks 1.
navneliste[1] = 'Donald Duck'
print(navneliste)

# Vi legger til et element på slutten av listen.
navneliste.append('Anderas')
print(navneliste)

# Vi sletter et element fra listen. (Rakk ikke dette i forelesning.)
del navneliste[3]
print(navneliste)

# Vi skriver ut lengden på listen (antall elementer).
print(len(navneliste))

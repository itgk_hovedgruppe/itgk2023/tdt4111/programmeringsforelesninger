# 6 - Uke 37

## Innhold
Her fortsetter vi litt på `if`, før vi går over på ukas tema:
- `list`: Lister med tall, tekster og mer.
- `løkke`:
    - `while`: Samme som `if`, men gjentar seg til påstanden er `False`.
    - `for`: Lar oss gå gjennom lister o.l. verdier, eller gjenta noe et spesifikt antall ganger.

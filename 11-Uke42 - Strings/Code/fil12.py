# En måte å representere et sjakkbrett i minnet
chessboard = [
    # a    b    c    d    e    f    g    h
    ['Q', 'h', 'b', 'k', 'q', 'b', 'h', 'r'],  # 8
    ['p', 'p', 'p', 'p', 'p', 'p', 'p', 'p'],  # 7
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],  # 6
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],  # 5
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],  # 4
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],  # 3
    ['P', 'P', 'P', 'P', 'P', 'P', 'P', 'P'],  # 2
    ['R', 'H', 'B', 'K', 'Q', 'B', 'H', 'R']   # 1
]

# Oppdatere oppe til venstre til 'Q': Altså flytte dronningen.
chessboard[0][0] = 'Q'


# Bedre måte å gjøre et trekk.
# Queen e1 -> a8
chessboard[0][0] = chessboard[7][4]
chessboard[7][4] = ' '


# Skrive ut sjakkbrett på en pen måte, med module pprint/pretty-print.
from pprint import pprint
pprint(chessboard)

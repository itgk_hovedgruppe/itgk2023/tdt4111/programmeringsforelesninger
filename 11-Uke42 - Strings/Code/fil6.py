# Sjekke om teksten starter med eller slutter med noe spesifikt.
print('Hello, World!'.startswith('Hello'))
print('Hello, World!'.endswith('World!'))


# Eksempel på nytteverdi (som ikke ble vist helt på forelesning).

# Lage filnavn (eller bruke os-modulen for å finne dem).
filnavn = 'rapport.pdf'

# Sjekke om filnavnet tilsier at det er en PDF-fil eller ikke.
if filnavn.endswith('.pdf'):
    print('PDF-fil')
else:
    print('Ukjent filtype. Bruk PDF!')

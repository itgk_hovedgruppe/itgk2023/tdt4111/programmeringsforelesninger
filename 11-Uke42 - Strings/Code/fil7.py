# Lagre en mailadresse.
tekst = 'abc@gmail.com'

# Splitte mailen i to, ved split-funksjonen.
brukernavn, domenenavn = tekst.split('@')

# str.split() splitter på mellomrom, tab/indent og new-line.
# str.split('@') splitter på '@'-symbolet. Dette funker også for abdre
# substrings. F.eks. str.split('noe tekst').

# Den returnerer en liste med elementer. I dette tilfellet:
# ['abc', 'gmail.com']. Siden vi vet at den er to lang (fordi alle emailer
# har nøyaktiv én @ i seg), kan vi hente ut begge elementere å lagre i
# variablene brukernavn og domenenavn.


# Bytte ut gmail.com med ntnu.no i stringen.
tekst = tekst.replace('gmail.com', 'ntnu.no')

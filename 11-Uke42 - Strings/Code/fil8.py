# Vi kan også stacke funksjonskall.
# Her bruker vi str.lower().count(). Det er lov.
print('The a the'.lower().count('the'))


# Her kjører jeg fire replaces på rad, på samme tekst.
tekst = 'hei'
tekst = tekst.replace('a', 'b').replace('r', 'q').replace('w', 't').replace('g', 'g')

# Gjorde dette at koden ble mer oversiktlig? Kanskje ikke. Men noen ganger er
# det nyttig å stacke funksjonskall etter hverandre.

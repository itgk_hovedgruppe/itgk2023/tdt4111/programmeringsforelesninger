# Nyttige moduler å vite om

# Math inkluderer en haug med nyttige mattefunksjoner.
import math

# Random inkluderer det du trenger for å generere tilfeldige tall.
import random

# OS-modulen gir deg tilgang til filsystemet, filnavnendring, liste filer og
# mapper, fjerne filer, etc.
import os

print(os.listdir())
# os.rename('spørmål.py', 'spørsmål.py')


# Sys(tem)-modulen gjør litt i samme kategori.
import sys

# Time-modulen gir deg tilgang til tiden (men ikke som dato)
import time

# Ta tiden på en ting
start = time.time()
time.sleep(0.3)  # Pause programmet i 0.3 s.
slutt = time.time()

# Finne hvor lang tid det tok.
tid = slutt - start
print(tid)


# Datetime hjelper deg med datoer. Ofte brukt sammen med time-modulen.
import datetime

print(datetime.datetime.now())

# Threading og multiprocessing er utenfor pensum, men nyttige hvis du må få
# masse parallelisering og effektiv kodekjøring på et tungt program.
import threading
import multiprocessing


### Eksterne moduler (må installeres for å brukes)

# Bruke clipboard. Altså ting du har kopiert kan endres.
import pyperclip

# NumPy (Numeric Python) er som math, bare mye mer og mye raskere. Det er en
# stor modul. Den er pensum, men ikke bruk 100 timer på å forstå den. Den lag
# en gjøre masse matte og mer effektivt. Mer om den senere? Eller se en
# YouTube-video på det.
import numpy
# Generelt sett importert på følgende måte:
import numpy as np

def f(x):
    return x**2

X = np.linspace(0, 1, 100)  # Lage et numpy-array 0 -> 1 med 100 verdier.
Y = f(X)  # Dette er lov med numpy-arrays!!! :D :D OMG!

# Lage grafer/plots/charts. Søk opp "matplotlib examples" for maaaaaange
# eksempler på dette. Også pensum.
import matplotlib
# Generelt sett importert på følgende måte:
import matplotlib.pyplot as plt

plt.plot(X, Y)
plt.show()  # FERDIG! :D That's it!

# Pandas er basically Excel i Python. Den lar oss jobbe med store mengder data,
# laste inn Excel-ark med en linje kode, analysere alt mulig, maaaaaasse. Men,
# ikke pensum. Stor modul.
import pandas
# Generelt sett importert på følgende måte:
import pandas as pd
xl = pd.ExcelFile('Resultatregnskap.xlsx')
df = xl.parse('Resultat')  # Bim bim bam bam lastet inn Excel-ark!
print(df)

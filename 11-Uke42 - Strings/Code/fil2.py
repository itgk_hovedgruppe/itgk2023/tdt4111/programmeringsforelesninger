# F-string-funksjonalitet
pi = 3.141592653
avrundet_pi = round(pi, 2)
ti = 10

# Vi kan runde av til 2 desimaler ved å legge til :.2f
# Vi kan tvinge kolonnebredde ved å legge til :8
# Vi kan gjøre begge ved å lege til :8.2f
print(f'{pi:8.2f}')
print(f'{ti:8.2f}')

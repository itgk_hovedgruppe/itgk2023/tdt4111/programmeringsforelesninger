# Oppgave: Lag om variablen mailer til sånn som vist i kommentaren på slutten.
mailer = 'abc@gmail.com;def@hotmail.no;ghi@ntnu.no;jkl@outlook.com'

# Bytte ut ';' med '\n' (new-line).
mailer2 = mailer.replace(';', '\n')

print(mailer2)


# Hva vi forventer å få ut.
'''
abc@gmail.com
def@hotmail.no
ghi@ntnu.no
jkl@outlook.com
'''

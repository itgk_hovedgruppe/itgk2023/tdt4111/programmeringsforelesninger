# Lage en enkel string-variabel og tvinge den til lowercase
tekst = 'Hello, World!'
print(tekst.lower())
# Resten kjøres direkte på stringen, i stedet for på string-variablen, men
# ellers er tankegangen det samme.

# Tvinge/konvertere tekst til lower-case, upper-case og title-case.
print('Hello, World!'.lower())
print('Hello, World!'.upper())
print('Hello, World!'.title())

# Sjekke om teksten er lower, upper eller title.
print('Hello, World!'.islower())
print('Hello, World!'.isupper())
print('Hello, World!'.istitle())

# Sjekke om teskten (alle tegnene i teksten) er alphabetic, numeric eller
# alphanumeric (bokstaver eller tall).
print('Hello, World!'.isalpha())
print('Hello, World!'.isnumeric())
print('Hello, World!'.isalnum())

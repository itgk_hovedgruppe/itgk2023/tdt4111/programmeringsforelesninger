# Vi kan lage string med ', ", ''', """
navn = 'Obama'
navn = "Obama"

navn = '''Obama'''
navn = """Obama"""

# Vi kan også bruke f-string med fnutt-tripletter
mail = f'''Hei, {navn}!
noe.
Hilsen Paul'''

print(mail)

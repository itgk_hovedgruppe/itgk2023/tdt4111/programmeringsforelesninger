# La variablen `navn`, uten f-string.
navn = 'Paul'

# Lage en email, ved å putte inn `navn` i stringen, ved å bruke f-string-ting.
# for navn in navneliste:
mail = f'Hei, {navn}! Vi bekrefter etelleannet.'

# Legge til en string i den første stringen.
mail += ' Hilsen IT'

# Skrive ut mailen.
print(mail)

'''
Husk!
En f-string er en vanlig string. Den er ikke annerledes etter den er laget. Den
bare lages på en annen måte.
'''

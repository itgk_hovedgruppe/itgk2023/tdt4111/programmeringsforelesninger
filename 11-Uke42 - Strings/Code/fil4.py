# Skal du ha en ' i en string: Bruk ".
quote = "It's me"

# Skal du har en " i en string: Bruk '.
quote2 = 'Han sa "hei" til meg'

# Skal du ha begge: Enten bruk ''' eller """, eller bruk \' eller \" for å
# "escape" symbolet. Dvs. at vi forteller Python at neste tegn er string, ikke
# kode.


'''
Potensiell risk (men mest fun-fact) med escaping:
Hvis du lar meg fylle inn navnet mitt som vist under ...
'''
navn = 'Paul'

'''
Så kan jeg velge følgende navn

Paul'\nimport os\nos.remove('/')'

Da kommer koden til å se ut som følger:

navn = 'Paul'
import os
os.remove('/')

om på Mac og Linux-språk betyr slett hele PCen. Ikke kjør koden selv!

I praksis er ikke dette relevant i Python, så lenge du ikke bruker funksjonene
exec eller eval. Og de er ikke pensum, så ikke bruk de.
'''

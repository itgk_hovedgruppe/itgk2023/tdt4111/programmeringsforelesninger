# Bruke den eksterne modulen pyperclip til å koble til clipboard. Altså minnet
# til PCen hvor kopierte ting ligger. Prøv det selv! :D
# Husk! Du må installere pyperclip ved å trykke Verktøy -> Administrere pakker
# (eller bruke pip, hvis du ikke bruker Thonny).

# Importere modulen.
import pyperclip

# "Lime inn" fra clipboard.
tekst = pyperclip.paste()

# Bytte ut og oppdatere teksten.
tekst = tekst.replace(';', '\n')

# "Kopiere" på nytt, tilbake til clipboard.
pyperclip.copy(tekst)

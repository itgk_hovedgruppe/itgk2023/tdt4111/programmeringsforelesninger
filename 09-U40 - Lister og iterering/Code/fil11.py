# Lage en liste med duplikate verdier.
liste = [2, 2, 2, 3, 2, 3, 4, 4, 2, 3, 4]

# Konvertere listen til et sett, og så tilbake til en liste. Dette fjerner
# duplikate verdier.
liste = list(set(liste))

print(liste)


'''
Ting sett er nyttige for:
- Setteori: Venndiagrammer og sånne ting. (Finne overlapp o.l.)
- Fjerne duplikate verdier i en liste (se eksempel over).
- Sjekke om et eller annet er i en liste. Se forklaring under.

x = 3
if x in liste: ...

sett = set(x)
if x in sett: ...

Den andre versjonen (x in sett) er ekstremt mye raskere enn den første, når
listen blir stor.
'''

# Sett

# Lage et sett direkte.
sett = {2, 3, 4}

# Lage en liste og konvertere den til et sett.
liste = [2, 2, 2, 3, 2, 3, 4, 4, 2, 3, 4]
sett = set(liste)

# Sett ser ut som en liste.
print(sett)

# Dict / dictionary

# ['Paul', 'Erna', 'Obama']

# Lage en dict, på en linje eller flere linjer. Ofte enklere å lese på flere.
# d = {'meg': 'Paul', 'statsminister': 'Erna', 'president': 'Obama'}
d = {
    'meg': 'Paul',
    'statsminister': 'Erna',
    'president': 'Obama'
}

# Hente ut d på indeks (eller key, som det heter) 'meg'.
print(d['meg'])

# Oppdatere d på indeks 'statsminister'
d['statsminister'] = 'Støre'
print(d)

# Legge til indeks 'konge' med verdi 'Harald'
d['konge'] = 'Harald'
print(d)

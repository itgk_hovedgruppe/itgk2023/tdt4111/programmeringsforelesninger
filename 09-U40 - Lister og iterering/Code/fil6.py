# Innebyggde funksjoner/metoder i liste

# Lage listen.
liste = [2, 3, 5, 7]
print(liste)

# Legge til element i listen.
liste.append(11)
print(liste)

# Oppdatere verdi i listen, basert på indeks.
liste[0] = 1
print(liste)

# Fjerne (og returnere) et element i listen. Indeks er optional.
tall = liste.pop(0)
print(liste)

# Fjerne element i listen.
del liste[0]
print(liste)

# Fjerne et element i listen (basert på verdi, ikke indeks).
liste.remove(5)
print(liste)

# Gange listen med 2 (duplisere).
liste = liste * 2
print(liste)

# Vi kan plusse sammen flere lister.
liste = liste + [13, 17, 19]
print(liste)

# Vi kan reversere rekkefølgen på en liste.
liste.reverse()
print(liste)

# Vi kan sortere elementene i en liste. Dette inkluderere alfabetisk
# rekkefølge. Vi kan også be den om å gjøre det i revers.
liste.sort()
# liste.sort(reverse=True)
print(liste)

# Default-verdi

# Slipper å sende inn g, med mindre den må være noe annet.
def gravitasjon(m, g=9.81):
    F = m * g
    return F


# Bruker default g
print(gravitasjon(10))

# Bruker en annen g (overskriver default)
print(gravitasjon(10, 6))

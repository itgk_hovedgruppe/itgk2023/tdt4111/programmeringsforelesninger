# Mer lister :D

# Lage en liste (med mange duplikater).
liste = [2, 3, 3, 5, 5, 5, 7, 7, 11]

# Lengden på listen.
print(len(liste))

# Antallet 5-tall vi finner i listen.
print(liste.count(5))

# Indeksen på verdien 11.
print(liste.index(11))

# Sjekke om tallet 7 finnes i listen.
print(7 in liste)

# Sjekke om teksten "the" finnes i stringen.
print('the' in "Don't ask me! Ask the king!")

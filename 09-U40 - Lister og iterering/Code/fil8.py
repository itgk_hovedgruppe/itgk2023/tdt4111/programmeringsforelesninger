# Liste vs tuppel 1

# Liste
navneliste = ['Paul', 'Moxnes']
navneliste[0] = 'Erna'
print(navneliste[0])
print(navneliste)


# Tuppel
navneliste = ('Paul', 'Moxnes')
# navneliste[0] = 'Erna'  # Det er ikke lov å endre en tuppel.
print(navneliste[0])
print(navneliste)

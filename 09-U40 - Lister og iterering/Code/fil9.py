# Returnerer tuppel: Returnere flere verdier fra en funksjon

# f mottar 1 verdi, og returnerer 2 verdier.
def f(x):
    y = 2 * x
    z = x**2
    return y, z  # (y, z) er en tuppel, selv om vi ikke skrev ().


# Resultat er av typen tuppel, med verdien (6, 9).
resultat = f(3)

# Her splitter vi opp tuppelen vi mottar, og lagrer de i de (globale)
# variablene y og z.
y, z = f(3)

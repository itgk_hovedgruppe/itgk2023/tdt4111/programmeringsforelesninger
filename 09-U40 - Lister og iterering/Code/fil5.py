# Iterables / itererbare objekter

liste = [2, 3, 5, 7]  # liste
liste = 'hei'  # string (har mange bokstaver)
liste = (2, 3, 5, 7)  # tuppel (likner på liste)

# Kan iterere over verdier i liste/string/tuppel
for tall in liste:
    print(tall)

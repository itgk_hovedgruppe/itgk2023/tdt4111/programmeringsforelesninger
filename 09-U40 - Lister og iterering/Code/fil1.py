# Globale og lokale variabler

# Lage globale konstaner (variabler som ikke kommer til å ha andre verdier).
g = 9.81
# m = 1000


# Lage funksjonen
def gravitasjon(m):
    # Finne svaret (basert på lokal m og global g)
    F = m * g
    return F


# Kjøre funksjonen med en masse
print(gravitasjon(1000))

# Verdien g er global, og finnes dermed også her nede.
print(g)

# print(m)  # Verdien m er lokal (_inni_ funksjonen gravitasjon).
# Den finnes ikke utenfor.

# Generelt sett: Hvis en verdi vil kunne være noe annet, send den inn som
# argument i funksjonen

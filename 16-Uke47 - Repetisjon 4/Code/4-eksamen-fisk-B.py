'''
Oppgave 3B: Fiskebutikk

Vi har tabellen `store` som inneholder informasjon om hvor mye fisk vi har av
hver type. Vi skal lage funksjonen `fish_amount`, hvor vi sender inn `store` og
`fish_type` (string), og funksjonen skal returnere antallet vi har av denne
typen fisk. Hvis fisken ikke finnes i store, skal vi returnere 0.

Vi kan gå ut i fra at hver type fisk bare står én gang i tabellen.
'''

store = [['torsk', 200], ['sei', 100]]

'''
store = {
    'torsk': 200
}

return store[fisk]
'''


def fish_amount(store, fish_type):
    # For hver rad i store.
    for rad in store:

        # Splitt raden.
        fisk, antall = rad

        # Hvis fisk i rad er samme som fisken vi ser etter, har vi en match.
        if fish_type == fisk:
            
            # Returner antallet.
            return antall
    
    # Hvis vi ikke har returnert enda, betyr det at vi ikke har funnet en
    # match. Altså er antallet 0.
    return 0


# >>> fish_amount(store, 'torsk')
# 200

print(fish_amount(store, 'torsk'))
print(fish_amount(store, 'sei'))
print(fish_amount(store, 'laks'))

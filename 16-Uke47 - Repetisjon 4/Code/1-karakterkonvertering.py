# En variabel, mange mulige verdier. I Python.
x = 3

if x == 0:
    print(0)
elif x == 1:
    print(1)
elif x == 2:
    print(2)
# osv ...


'''
# Hvorvan vi kunne gjort det i f.eks programmeringsspråket C/C++ eller Java.
# Bruke en "switch".
switch (x)
{
    case 0:
        print()
    case 1:
        print()
}
'''


# Men Python har ikke en switch. Hva gjør vi da?
# Jo, vi kan bruke en dictionary for lookup.

# Konverter bokstav-karakter til tall-karakter.
# A, B, C, ... til 5, 4, 3, ...

def bokstav_til_tall_v1(karakter):
    '''Med if, som vist tidligere.'''
    if karakter == 'A':
        return 5
    elif karakter == 'B':
        return 4
    elif karakter == 'C':
        return 3
    elif karakter == 'D':
        return 2
    elif karakter == 'E':
        return 1
    else:
        return 0
    

def bokstav_til_tall_v2(karakter):
    '''Med en dict.
    Litt samme logikk som vlookup i Excel. Vi har et oppslagsregister.'''
    lookup = {
        'A': 5,
        'B': 4,
        'C': 3,
        'D': 2,
        'E': 1,
        'F': 0
    }
    return lookup[karakter]


def bokstav_til_tall_v3(karakter):
    '''Mer kompakt versjon av den forrige.'''
    return {'A': 5, 'B': 4, 'C': 3, 'D': 2, 'E': 1, 'F': 0}[karakter]


# Skrive ut resultat fra alle versjonene våre.
print(bokstav_til_tall_v1('B'))
print(bokstav_til_tall_v2('B'))
print(bokstav_til_tall_v3('B'))

# sum fra i=0 til 4, av x^i
# (Se filen `2-sum.md` for mer om summen.)
# 3^0 + 3^1 + 3^2 + 3^3 + 3^4 = 1 + 3 + 9 + 27 + 81 = 121

x = 3
summen = 0

for i in range(0, 5):
    summen += x**i

print(summen)

'''
Legg merke til at veldig lite av denne koden må byttes ut, hvis vi skal bruke
en annen sum-funksjon.
- x er variabelen (tenk `f(x)` o.l.).
- range(0, 5) er intervallet vi summerer over. i=0 -> 4 gir 0-5, siden range
    ikke inkluderer det siste tallet.
- x**i er funksjonen vi summerer.

Vi kan endre tallet `x`, vi kan endre 0 og 5 til det vi skal, og endre `x**i`.
Så har vi en generell sum-funksjon.
'''

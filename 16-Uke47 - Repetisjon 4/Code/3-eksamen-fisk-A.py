'''
Oppgave 3A: Båters regnummer

Her skal vi lage en funksjon som tar inn som skal godkjenne eller ikke
godkjenne registernummer/skiltnummer på båter. For å være godkjente, må de
følge følgende regler.

1. Starte med en bokstav
2. Følge med et eller to siffer
3. Avslutte med en eller to bokstaver.

Lag funksjonen `check_registration`, som tar inn et registernummer (string), og
returnerer `True` hvis det er godkjent, og `False` hvis det ikke er godkjent.

Om løsningen:
- v1 funker bare hvir regnummeret er 5 tegn langt.
- v2 ga vi opp på, fordi vi innså at oppgaven var litt vanskeligere enn tenkt.
- v3 funker.
'''


def check_registration_v1(regnummer):
    if not regnummer[0].isalpha():
        return False
    
    if not regnummer[1:3].isnumeric():
        return False
    
    if not regnummer[4:6].isalpha():
        return False
    
    return True



def check_registration_v2(regnummer):
    if len(regnummer) == 5:
        if not regnummer[0].isalpha():
            return False
        
        if not regnummer[1:3].isnumeric():
            return False
        
        if not regnummer[4:6].isalpha():
            return False
        
        return True
    elif len(regnummer) == 4:
        # osv ...
        pass
        

# 01234
# A11AA
# A1AA
# A11A
# A1A


# len(regnummer), 3 - 5


def check_registration_v3(regnummer):
    if not (3 <= len(regnummer) <= 5):
        return False
    
    if len(regnummer) == 3:
        return (
            regnummer[0].isalpha() and
            regnummer[1].isnumeric() and
            regnummer[2].isalpha()
        )

    elif len(regnummer) == 5:
        return (
            regnummer[0].isalpha() and
            regnummer[1:3].isnumeric() and
            regnummer[3:5].isalpha()
        )
    
    elif len(regnummer) == 4:
        return (
            regnummer[0].isalpha() and
            regnummer[1].isnumeric() and
            regnummer[2].isalnum() and
            regnummer[3].isalpha()
        )


print(check_registration_v3('A12BC'))
print(check_registration_v3('ABBA'))

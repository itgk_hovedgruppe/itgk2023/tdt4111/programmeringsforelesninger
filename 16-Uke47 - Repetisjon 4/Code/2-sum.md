Dette er summen beskrevet i filen `2-sum.py`.

$$ \Sigma_{i=0}^{4} x^i $$

Pakket ut av summen, blir det følgende.

$$ x^0 + x^1 + x^2 + x^3 + x^4 $$

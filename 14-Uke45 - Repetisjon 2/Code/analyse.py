'''
1. Les inn tekst fra fil
1b. Fiks tekst
2. Splitte teksten til en liste med ord
3. Telle antallet av hvert ord
4. Hente ut mest brukte ord / sortere rekkefølgen på dict
5. Plot listen (eller de 10 største)
'''

import matplotlib.pyplot as plt


## Definere alle funksjonene vi trenger

def les_fra_fil():  # 1
    f = open('bible.txt', 'r')
    tekst = f.read()
    f.close()
    return tekst

def fiks_tekst(tekst):  # 1b
    tekst = tekst.lower()

    # Fjerne ikke-bokstaver
    '''
    symboler = '.,:;-_/\\#!?+"\'0123456789'
    for symbol in symboler:
        tekst = tekst.replace(symbol, ' ')
    '''
    
    ny_tekst = ''
    for bokstav in tekst:
        if bokstav.isalpha():
            ny_tekst += bokstav
        else:
            ny_tekst += ' '

    '''
    tekst = tekst.replace(',', ' ')
    tekst = tekst.replace('.', ' ')
    tekst = tekst.replace(';', ' ')
    tekst = tekst.replace(':', ' ')
    tekst = tekst.replace('-', ' ')
    tekst = tekst.replace('_', ' ')
    tekst = tekst.replace('\'', ' ')
    tekst = tekst.replace('"', ' ')
    tekst = tekst.replace('!', ' ')
    tekst = tekst.replace('?', ' ')
    tekst = tekst.replace('+', ' ')
    '''

    return ny_tekst

def splitt_til_ord(tekst):  # 2
    return tekst.split()

def tell_antall_ord(ordliste):  # 3
    '''
    {
        'the': 6123,
        'and': 1343,
        ...
    }
    '''
    ordfrekvenser = {}
    for Ord in ordliste:
        if Ord in ordfrekvenser:
            ordfrekvenser[Ord] += 1
        else:
            ordfrekvenser[Ord] = 1
    return ordfrekvenser

def sorter_dict(ordfrekvenser):  # 4
    return {k: v for k, v in sorted(ordfrekvenser.items(),
                                    reverse=True,
                                    key=lambda item: item[1])}

def plot_ord(ordfrekvenser):  # 5
    ordliste = ordfrekvenser.keys()
    frekvenser = ordfrekvenser.values()
    
    ordliste = list(ordliste)[:10]
    frekvenser = list(frekvenser)[:10]

    plt.bar(ordliste, frekvenser)
    plt.show()


## Hovedprogrammet

tekst = les_fra_fil()  # 1
# print(tekst[:100])

tekst = fiks_tekst(tekst)  # 1b
# print(tekst[:100])

ordliste = splitt_til_ord(tekst)  # 2
# print(ordliste[:20])

ordfrekvenser = tell_antall_ord(ordliste)  # 3
# print(ordfrekvenser)

ordfrekvenser = sorter_dict(ordfrekvenser)  # 4
# print(ordfrekvenser)

plot_ord(ordfrekvenser)  # 5

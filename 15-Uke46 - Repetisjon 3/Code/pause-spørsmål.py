## Spørsmål fra pausen

# Gitt funksjonen f
def f(x):
    return x**2

# Og listen X
X = [0, 1, 2, 3, 4, 5]

# Her er noen forskjellige metoder å lage listen Y,
# hvor Y = f(X)

## Metode 1 - Lage tom Y, for-loop igjennom elementvis.
Y = []

for x in X:
    y = f(x)
    Y.append(y)

# Metode 2 - Bare bruk map-funksjonen. :D
Y = map(f, X)

# Metode 3 - Bruke list-comprehension.
Y = [f(x) for x in X]

# Resultatet blir mer eller mindre det samme for alle tre løsningene.
print(Y)

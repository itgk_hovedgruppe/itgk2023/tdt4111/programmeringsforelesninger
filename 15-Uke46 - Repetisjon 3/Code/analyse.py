'''
Stort program. Vi lage. Men først, planlegge.


1. Les inn tekst fra fil
1b. Fiks tekst
2. Splitte teksten til en liste med ord
3. Telle antallet av hvert ord
4. Hente ut mest brukte ord / sortere rekkefølgen på dict
5. Plot listen (eller de 10 største)

6. (Bonus) Lagre data/tabell som csv-fil
7. (Bonus) Filtrere vekk ord som ikke er 6 bokstver lange
    Denne gjør vi mellom punkt 2 og 3.
'''

import matplotlib.pyplot as plt


## Definere alle funksjonene vi trenger

def les_fra_fil():  # 1
    f = open('bible.txt', 'r')
    tekst = f.read()
    f.close()
    return tekst


def fiks_tekst(tekst):  # 1b
    tekst = tekst.lower()

    # Fjerne ikke-bokstaver
    '''
    # Replace manuelt
    tekst = tekst.replace(',', ' ')
    tekst = tekst.replace('.', ' ')
    tekst = tekst.replace(';', ' ')
    tekst = tekst.replace(':', ' ')
    tekst = tekst.replace('-', ' ')
    tekst = tekst.replace('_', ' ')
    tekst = tekst.replace('\'', ' ')
    tekst = tekst.replace('"', ' ')
    tekst = tekst.replace('!', ' ')
    tekst = tekst.replace('?', ' ')
    tekst = tekst.replace('+', ' ')
    '''

    '''
    # Replace med løkke
    symboler = '.,:;-_/\\#!?+"\'0123456789'
    for symbol in symboler:
        tekst = tekst.replace(symbol, ' ')
    '''

    # Fjerne ikke-bokstav-tegn
    ny_tekst = ''
    for bokstav in tekst:
        if bokstav.isalpha():
            ny_tekst += bokstav
        else:
            ny_tekst += ' '

    return ny_tekst


def splitt_til_ord(tekst):  # 2
    return tekst.split()


def tell_antall_ord(ordliste):  # 3
    '''
    {
        'the': 6123,
        'and': 1343,
        'in': 1,
        ...
    }
    '''
    ordfrekvenser = {}
    for Ord in ordliste:
        if Ord in ordfrekvenser:
            ordfrekvenser[Ord] += 1
        else:
            ordfrekvenser[Ord] = 1
    return ordfrekvenser


def sorter_dict(ordfrekvenser):  # 4
    # return {k: v for k, v in sorted(ordfrekvenser.items(),
    #                                 reverse=True,
    #                                 key=lambda item: item[1])}
    # [
    #     [value, key],
    #     [value, key],
    #     ...
    # ]
    tabell = []
    for key, value in ordfrekvenser.items():
        ny_rad = [value, key]
        tabell.append(ny_rad)

    tabell = sorted(tabell, reverse=True)
    # tabell.sort()

    ny_dict = {}
    for rad in tabell:
        value, key = rad
        ny_dict[key] = value

    return ny_dict


def plot_ord(ordfrekvenser):  # 5
    ordliste = ordfrekvenser.keys()
    frekvenser = ordfrekvenser.values()
    
    ordliste = list(ordliste)[:100]
    frekvenser = list(frekvenser)[:100]

    plt.bar(ordliste, frekvenser)
    plt.savefig('result.png')
    plt.show()


def lagre_til_csv(ordfrekvenser):
    '''
    # Hvordan vi vil at csv-stringen skal se ut:
    ord, frekvens
    the, 6123
    of, 4567
    ...

    '''

    # 1. Bygge opp string
    tekst = 'ord, frekvens\n'  # Lage titler.
    for Ord, antall in ordfrekvenser.items():
        tekst += f'{Ord}, {antall}\n'  # Legge til en og en linje.
    
    # 2. Lagre string til fil
    f = open('ordfrekvenser.csv', 'w')
    f.write(tekst)
    f.close()


## Hovedprogrammet

tekst = les_fra_fil()  # 1
# print(tekst[:100])

tekst = fiks_tekst(tekst)  # 1b
# print(tekst[:100])

ordliste = splitt_til_ord(tekst)  # 2
# print(ordliste[:20])
# print(len(ordliste))  # ca. 800 000 ord

# Filtrere vekk ord som ikke er 6 lange.
# (Lagde denne utenfor funksjon.)
ny_ordliste = []
for Ord in ordliste:
    if len(Ord) == 6:
        ny_ordliste.append(Ord)

ordfrekvenser = tell_antall_ord(ordliste)  # 3
# print(ordfrekvenser)

ordfrekvenser = sorter_dict(ordfrekvenser)  # 4
# print(ordfrekvenser)

plot_ord(ordfrekvenser)  # 5

lagre_til_csv(ordfrekvenser)  # 6

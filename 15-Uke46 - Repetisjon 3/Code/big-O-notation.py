# En annen måte å konstruere tell-antall-ord-funksjonen.


# Eksempel-liste (800_000-ish lang)
ordliste = ['in', 'the', 'beginning', 'god', 'created', 'the', 'heaven', 'and', 'the', 'earth']

# Lag et sett (liste-ish med bare unike ord) (13_000-ish lang)
ordsett = set(ordliste)

# For hvert unike ord...
for Ord in ordsett:  # 800_000 -> 13_000
    # ... tell antallet for dette ordet, og lagre i dict
    ordfrekvenser[Ord] = ordliste.count(Ord)


'''
Denne virker som den burde være like rask, om ikke raskere enn
den forrige. Den er, derimot, utrolig mye tregene. Hvorfor?
Jo, fordi .count vil gå igjennom hele listen (800 000+ ord)
og den vil gjøre det for hvert eneste unike ord. Ergo vil vi
gjøre noe sånt som 13 000 * 800 000 ~= 10_000_000 sammenlikninger.
Myyyye tregere.
Her var vi innom algoritmisk effektivitet og såvidt innom Big-O
notation. Dette forklarer noe om hvordan algoritmers tidsbruk
vokser med flere elementer i listen.
'''

# Vi oppdaterer funksjonen i en egen fil



# Lage en eksempel-ordfrekvens-dict, for testing purposes.
ordfrekvenser = {
    'and': 1343,
    'in': 1,
    'the': 6123
}


# Oppdatert funksjon
def sorter_dict(ordfrekvenser):  # 4
    '''
    # Koden vi hadde:
    return {k: v for k, v in sorted(ordfrekvenser.items(),
                                    reverse=True,
                                    key=lambda item: item[1])}
    
    # Hvordan vi vil sette opp den midlertidige tabellen vår.
    [
        [value, key],
        [value, key],
        ...
    ]
    '''

    # dict -> tabell, bytte kolonner
    tabell = []
    for key, value in ordfrekvenser.items():
        ny_rad = [value, key]
        tabell.append(ny_rad)

    # Sortere (reversert)
    tabell = sorted(tabell, reverse=True)
    # tabell.sort(reverse=True)

    # tabell -> dict, bytte kolonner tilbake
    ny_dict = {}
    for rad in tabell:
        value, key = rad
        ny_dict[key] = value

    return ny_dict


# Skrive ut resultat med test-dict, og se at vi får forventede resultater.
print(sorter_dict(ordfrekvenser))

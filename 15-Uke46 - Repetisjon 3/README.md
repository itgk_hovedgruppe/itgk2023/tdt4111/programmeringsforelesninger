# Innhold

- Info
    - Kodekveld 1 (og 2)
    - Hvordan øve til eksamen
    - Hva vi har tilgang til på eksamen

- Bibel
    - Summary fra forrige gang
    - v1 vs v2
    - Alternativ (ikke-kopi) versjon av sortering
    - Plot, savefig, etc.
    - Lagre til csv
    - PIN-oppgave
    - (Språkanalyse og Zipfs lov)

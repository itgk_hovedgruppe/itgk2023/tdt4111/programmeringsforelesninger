# Rakk såvidt denne. Mer om dette i forelesningsnotatene.

# For alle x-verdier i 0, 1, 2
for x in range(3):
    # (for hver x-verdi, gjør:) for alle y-verdier i 0, 1, 2
    for y in range(3):
        # Skriv ut x og y.
        print(x, y)

'''
Denne vil skrive ut alle kombinasjoner av x og y. Altså:
00, 01, 02, 10, 11, 12, 20, 21, 22 (men ikke akkurat som dette).

Hvordan er dette nyttig? Jo, hvis vi lager en tabell med 3x3 elementer:
'''

tabell = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

'''
Så vil indeksene til alle elementene være innenfor x, y element i {0, 1, 2}.
hashtag matte

Basically, indeksene er som følger:
'''

tabell = [
    ['00', '01', '02'],
    ['10', '11', '12'],
    ['20', '21', '22']
]

'''
Så hvis vi skal iterere over alle elementene i tabell, kan vi gjøre noe som:

for x in range(3):
    for y in range(3):
        print(tabell[x][y])

Men masse mer om dette senere. Ikke helt pensum så tidlig.
'''

# Iterere gjennom en liste med indekser i stedet for "vanlig" for-iterering.

# index: 0  1  2  3   4   5   6   7
liste = [2, 3, 5, 7, 11, 13, 17, 19, 23]
N = len(liste)

# i vil ha verdiene til indeksen til listen.
for i in range(N):  # range(len(liste))
    # print(liste[i])
    print(f'Primtall nummer {i+1} er {liste[i]}')

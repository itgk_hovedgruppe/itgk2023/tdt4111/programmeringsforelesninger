'''
Denne funker ikke. Ikke uten å installere matplotlib-modulen med pakke-greia.
Det kan gjøres ved å gå til:
Hovedmeny -> Verktøy -> Administrere pakker -> Søk -> "matplotlib" -> Installer
Da vil den installere noen ting, og du skal kunne bruke den.
'''


# Importere modulen (og kalle den "plt")
import matplotlib.pyplot as plt


# Lage en funksjon.
def f(x):
    y = x**2
    return y


# Lage X-verdier (med range) og en tom Y-verdier.
X = range(101)
Y = []

# Samme som forrige eksempel: Y = f(X), men elementvis.
for x in X:
    y = f(x)
    Y.append(y)

# Plotte (lage graf av) funksjonen f.
plt.plot(X, Y)
plt.show()

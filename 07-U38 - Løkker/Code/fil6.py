'''
Oppgave: Fra tidligere kode, gjør at den runder av X-verdiene i stedet for å
kjøre de gjennom f. Altså, i stedet for å gjøre følgende elementvis

Y = f(X)

vil vi gjøre følgende elementvis

Y = round(X)

Løsning under.
'''

# Funksjonen fra forrige. Trenger egentlig ikke denne.
def f(x):
    y = x**2
    return y


# Lage listene.
X = [1.2, 3.4, 5.6, 7.8, 9.10]
Y = []

# Elementvis gjennomgang.
for x in X:
    # Bruke "round" i stedet for "f". Dette er det eneste som måtte endres.
    y = round(x)  # f(x)
    # y = round(y)
    Y.append(y)

# Skrive ut resultat.
print(Y)

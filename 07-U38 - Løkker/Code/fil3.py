# Vi kan også iterere over andre datatyper med flere verdier.
# En tekst består av flere elementer.

# Lag en tekst
tekst = 'Askeladd'

# Iterer over hver bokstav i teksten.
for bokstav in tekst:
    print(bokstav, end=' ')

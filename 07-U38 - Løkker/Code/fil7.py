'''
Range

Range-funksjonen kan ta i mot enten 1, 2 eller 3 argumenter/verdier. Avhengig
av hvor mange den får, har den forskjellig oppførsel. Dette er forklart og
illustrett under.

range(N) = 0, 1, 2, 3, ..., N-1
range(4) = 0, 1, 2, 3
range(stop)

range(A, B) = A, A+1, A+2, ..., B-1
range(3, 8) = 3, 4, 5, 6, 7
range(start, stop)

range(A, B, C) = A, A+1C, A+2C, ..., B-1
range(3, 13, 2) = 3, 5, 7, 9, 11
range(start, stop, steglengde)
'''


# Antall argumenter: 1
for i in range(10):
    print(i, end=' ')
print()


# Antall argumenter: 2
for i in range(3, 8):
    print(i, end=' ')
print()


# Antall argumenter: 3
for i in range(3, 13, 2):
    print(i, end=' ')
print()

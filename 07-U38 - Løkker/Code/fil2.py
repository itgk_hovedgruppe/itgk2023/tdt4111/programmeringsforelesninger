# Lag en liste med f.eks. navn
navneliste = ['Per', 'Pål', 'Espen']

# Iterer over listen, og skriv ut hvert element. Hvert navn, i dette tilfellet.
for navn in navneliste:
    print(navn, end=' ')

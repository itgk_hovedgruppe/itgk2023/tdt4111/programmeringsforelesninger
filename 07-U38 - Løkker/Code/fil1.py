'''
# Slippe ut av fengsel i Monopol

Dette er et kort while-eksempel som inkluderer import av en modul.

Når vi skal bruke kode som Python-folka eller andre programmerer har skrevet
men som ikke ligger helt tilgjengelig i vanlig kode, kan vi importere det.
Mer om import og moduler senere.

Kortversjonen er at hvis modulen heter "random", kan vi skrive "import random"
i starten av filen. Da kan vi gjøre random.randint for å bruke
randint-funksjonen som er en del av modulen random. Det er også flere måter å
importere ting på. Mer om det senere.

Funksjonen randint gir tilbake et tilfeldig tall mellom a og b (inklusivt):
x = random.randint(a, b)  # x er et tilfeldig tall a <= x <= b.
'''

# Importere random.
import random


# Kast terning og skriv ut hva vi fikk.
terning1 = random.randint(1, 6)
terning2 = random.randint(1, 6)
print(terning1, terning2)

# Initialiser en antall_kast-variabel.
antall_kast = 1

# Gjenta kode så lenge kastene ikke er like.
while terning1 != terning2:
    # Kast igjen og skriv ut hva vi fikk.
    terning1 = random.randint(1, 6)
    terning2 = random.randint(1, 6)
    print(terning1, terning2)
    
    # Inkrementer antall_kast hver gang vi kaster på nytt.
    antall_kast += 1

# Skriv ut resultat.
print(f'Antall kast: {antall_kast}')

'''
Dette eksempelet er veldig nyttig! Du kan kopiere det og endre f, eller andre
småting, og få den til å gjøre det meste du trenger.
Den tar en liste med verdier (X) og kjører hvert element gjennom funksjonen (f)
og lager den nye listen (Y).
'''


# Lage funksjonen f(x) = x^2
def f(x):
    y = x**2
    return y

# Lage en liste med X-verdier (stor bokstav for liste i Paul-kode)
X = [0, 1, 2, 3, 4, 5, 6]

# Lage Y-verdiene manuelt. Kjedelig.
# Y = [0, 1, 4, 9, 16, 25, 36]

# Kjøre X-verdiene gjennom funksjonen som en liste. Går ikke :(
# (Men går hvis en bruker modulen numpy :D)
# Y = f(X)

# Lage en tom Y-liste.
Y = []

# Iterere gjennom alle x-verdiene i X.
for x in X:
    # Finne en og en y-verdi.
    y = f(x)

    # Legge den til i Y-listen.
    Y.append(y)


# Skrive ut resultatet.
print(Y)

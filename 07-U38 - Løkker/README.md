# 7 - Uke 38

## Innhold
Forrige uke ble vi ferdige med `if` og `while`. Denne uken fortsetter vi med `for`, og ser litt mer på funksjoner:

- Repetisjon: while
- `for`: Lister med tall, tekster og mer.
- Funksjoner

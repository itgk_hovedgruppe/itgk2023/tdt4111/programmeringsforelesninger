# Typisk setup for småprogrammer: Setup, utregning og utskrift av resultat.

# Setup
x = 17  # Sette opp x
y = 234  # Sette opp y

# Utregning
z = y - x

# Skrive ut resultat
# print(x)
# print(y)
print(z)

# Om type-funksjonen i kode ("if" er litt frem i pensum)

# Lager x som er float.
x = 3.0

# Hvis typen til x er int (noe den ikke er)
if type(x) == int:
    print("x er int")

# Hvis typen til x er int (noe den er)
if type(x) == float:
    print("x er float")

const util = require("util");

console.log(util.inspect([] + {}));
console.log(util.inspect({} + []));
console.log(util.inspect({} + {}));
console.log(util.inspect([] + []));

console.log(util.inspect(2 + "2"));
console.log(util.inspect(2 - "2"));

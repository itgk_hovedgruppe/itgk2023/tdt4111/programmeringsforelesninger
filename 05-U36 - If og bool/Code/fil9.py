## Disse to funksjonene gjør det samme som forrige.

# v1: return stopper funksjonen med en gang. Altså vil return True
# bare skje hvis return False ikke skjer. Dermed har vi laget en
# slags hjemmelaget if-else-struktur.

def er_positivt(tall):
    if tall < 0:
        return False
    return True


# v2: Påstanden er tall < 0. Funksjonen vår gjør følgende:
# Hvis påstand er True, return False
# Hvis påstand er False, return True
# Eller med andre ord returnerer vi det motsatte av påstanden.
# Derfor kan vi bare returnerer not påstand.

def er_positivt(tall):
    return not tall < 0


# v3 (ikke med i forelesning): Vi kan faktisk bare reversere
# testen, og slippe not. Sparer tid på å kode. Ikke alltid like
# lett å lese.

def er_positivt(tall):
    return tall >= 0


print(er_positivt(123))

# Setter opp variabler
lagring = 2000  # GB
pris = 18_000  # kr

# Utfører test
akseptabel_laptop = (pris < 13_000) or (lagring > 1000)

# Skriver ut resultat
print(akseptabel_laptop)

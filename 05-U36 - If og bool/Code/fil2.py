# Krav1: Må være en Mac
# Krav2: Må ha mer enn 1000 GB lagring
# Krav3: Laptopskjermstørrelse på 16 tommer eller mer
# Krav4: Mer enn 10t batterilevetid, eller minst 90W lader
# Krav5: Må ikke være brukt

# Sette opp laptop-variabler
er_mac = True  # HUSK! Stor bokstav i True og False!
er_brukt = False
lagring = 2000  # GB
skjermstørrelse = 15  # tommer
pris = 14000
batterilevetid = 8  # t
ladereffekt = 140  # W

# Løse/teste ett og ett krav
krav1 = er_mac
krav2 = (lagring > 1000)
krav3 = (skjermstørrelse >= 16)
krav4 = (batterilevetid > 10) or (ladereffekt >= 90)
krav5 = not er_brukt

# Kombinere kravene
akseptabel_laptop = krav1 and krav2 and krav3 and krav4 and krav5

# Hvordan dette ville sett ut uten å dele opp i delkrav.
# akseptabel_laptop = er_mac and (lagring > 1000) and (skjermstørrelse >= 16) and ((batterilevetid > 10) or (ladereffekt >= 90)) and not er_brukt

# Skriv ut resultat
# print(akseptabel_laptop)

# Velge utskrift basert på resulatet
if akseptabel_laptop:
    print('Kjøp den!')
else:
    print('Ikke kjøp den! :(')

## Denne koden gjør det samme som den forrige, men testene er
# snutt på hodet. Dermed ble strukturen på if-en mye mer
# komplisert. Dette kan unngås ved å tenke litt på hvordan if-ene
# kan gjøres. Bruk gjerne flow charts!


alder = 15

if alder >= 18:
    if alder >= 20:
        print('spriit woo')
    else:
        print('øl only')
else:
    print('For ung')

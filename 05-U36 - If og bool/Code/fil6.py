påstand = False


# Skummelt: Lage variabel i en if, som kanskje ikke kjøres
if påstand:
    x = 4

# Tryggere: Sørge for at alle if-ene lage y.
# Da vet vi at den finnes.
if påstand:
    y = 4
else:
    y = 1

# Også tryggere: Lage variablen utenfor if-en, og oppdatere if...
z = 1
if påstand:
    z = 4

# x finnes ikke hvis påstand er False.
# Dermed kan programmet krasje her.
print(x, y, z)

# Sette opp variabel
alder = 15


# Hvis alder < 18: Ikke kjøpe alkohol
if alder < 18:
    print('Du kan ikke kjøpe alkohol')

# Hvis første if ikke trigger, men alder < 20: Øl, men ikke sprit
elif alder < 20:
    print('Du kan kjøpe øl, men ikke sprit')

# Hvis ingen av de første if-ene trigger, må du være 20 eller mer.
else:
    print('Du kan kjøpe øl og sprit')

# Funksjon som tester om tall er positivt.
# Hvis tall < 0 er det negativt. Dermed returnerer vi False.
# Hvis ikke, må det være positivt (0 er positivt?).
# Dermed returnerer vi True.

def er_positivt(tall):
    if tall < 0:
        return False
    else:
        return True

print(er_positivt(-123))

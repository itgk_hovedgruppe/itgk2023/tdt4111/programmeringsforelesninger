# Lage antall-skuddår-variabel.
antall_dager_i_februar = 28

# Skrive om det er skuddår eller ikke.
skuddår_i_år = True

# Valg: Hvis det er skuddår, skal det være 29, ikke 28 dager.
if skuddår_i_år:
    antall_dager_i_februar = 29

# Skriv ut resultatet
print(antall_dager_i_februar)

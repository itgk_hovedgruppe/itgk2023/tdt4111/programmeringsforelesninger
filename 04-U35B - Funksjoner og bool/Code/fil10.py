# Boolske verdier og tester
x = (3 < 4)  # True

y = 5
y_mindre_enn_7 = (y < 7)  # True

y_er_lik_3 = (y == 3)  # False

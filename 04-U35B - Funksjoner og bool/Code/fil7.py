# Flere operatorer
x = 4

# Enkel måte å inkrementere x med 1.
x = x + 1

# Mer kompakt måte å inkrementere x med 1.
x += 1

# Denne syntaksen gjelder alle operatorene vi har sett på så langt.
# Men ikke alle er like nyttige.
x += 1
x -= 1
x *= 2
x /= 2
x **= 2
x //= 1.3
x %= 2

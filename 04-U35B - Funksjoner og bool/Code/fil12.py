# Setup
pris = 100000

# Utregning
# 8000 < pris < 15000
resultat = (pris > 8000) and (pris < 15000)

# Skrive ut resultat, basert på resultatet av testen (neste ukes pensum).
if resultat:
    print('Laptopen er innafor :D')
else:
    print('Laptopen er ikke bra')

# f-string

# Lage variabler
x = 4
y = 5
z = x + y

# Flere måter å skrive ut svaret på, som et ordentlig mattestykke.
print('4 + 5 = 9')  # Hardcoding: Ikke basert på verdiene i variablene.
print(x, '+', y, '=', z)  # Komma
print('Hvis vi plusser sammen', x, 'og', y, 'så får vi', z)  # Mer komma
print('Hvis vi plusser sammen x og y så får vi z.')  # Det vi har lyst på
print(f'Hvis vi plusser sammen {x} og {y} så får vi {z}.')  # Bruk av f-string.

# Samme funksjonen som i forrige fil, men med bedre funksjons-/variablnavn.
def beregn_volum(høyde, bredde, lengde):
    volum = høyde * bredde * lengde
    return volum


print(beregn_volum(2, 3, 4))

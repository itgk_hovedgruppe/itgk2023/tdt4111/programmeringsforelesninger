# Funksjonen for å regne ut volumet fra hølyde, bredde og lengde.
def V(h, b, l):
    return h*b*l

# Kjøre og skrive ut.
print(V(2, 3, 4))

# Kjøre og mellomlagre i variablen volum, før vi skriver ut.
volum = V(2, 3, 4)
print(volum)

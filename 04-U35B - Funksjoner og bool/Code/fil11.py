# Kjøpe laptop. Må ikke være for dyr, men heller ikke for billig.

# Setup
pris = 10000

# Utregning
# 8000 < pris < 15000
resultat = (pris > 8000) and (pris < 15000)

# Skrive ut resultat
print(resultat)

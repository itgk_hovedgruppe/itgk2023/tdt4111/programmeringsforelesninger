# Funksjonen f(x) = 2x
def f(x):
    y = 2 * x
    return y


# Kjør/kall funksjonen og skriv resultatet ut direkte.
print(f(12))

# Kjør/kall funksjonen og lagre i variablen verdi, før vi skriver det ut.
verdi = f(3)
print(verdi)

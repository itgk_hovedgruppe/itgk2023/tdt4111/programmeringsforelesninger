# Funksjonen f(x) = 5x^2 - 3x + 1
def f(x):
    y = 5*x**2 - 3*x + 1
    return y


# Kjøre og skrive ut verdien
print(f(6))

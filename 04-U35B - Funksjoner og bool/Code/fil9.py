# Setup
innskudd = 10000  # kroner
rente = 0.05
antall_år = 13

# Utregning
sluttsum = innskudd * (1 + rente)**antall_år

# Skrive ut svar
print(sluttsum)

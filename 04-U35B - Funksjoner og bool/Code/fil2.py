# Print er en (innebygget) funksjon, som ikke har en return-verdi.
# Derfor gir det ikke mening å skrive "x = print(...)". Vi får ingen verdi.
x = print(3)
print(x)

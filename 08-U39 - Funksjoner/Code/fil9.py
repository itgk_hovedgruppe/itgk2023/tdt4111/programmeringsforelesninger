# Importer modulen/bibloteket random, som kan generere tilfeldige tall.
import random


# Bruke random sin randint-funksjon.
# I dette tilfellet gir den samme resultat som av å kaste en terning.
terningkast = random.randint(1, 6)
print(terningkast)

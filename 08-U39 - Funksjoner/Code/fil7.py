'''
Maks lån: 5x årslønn
Egenandel: 0.15 * lån
'''

# Returnerer hvor stort lån en kan ta med følgende årslønn og egenandel.
def maks_boliglån(årslønn, egenandel):
    maks_basert_på_lønn = 5 * årslønn
    maks_basert_på_egenandel = egenandel / 0.15
    svar = min(maks_basert_på_lønn, maks_basert_på_egenandel)
    return svar


print(maks_boliglån(500000, 350000))

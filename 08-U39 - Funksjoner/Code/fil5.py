# Definere en enkel funksjon f
def f(x):  # Funksjonen heter f. Den tar i mot/forventer ett parameter: x.
    # Output fra funksjonen returneres: x**2
    return x**2

# Kjøre funksjonen med input 3 (som gir output 9)
y = f(3)
print(y)

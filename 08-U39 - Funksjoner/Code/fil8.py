# Returnerer hva som er krav av årslønn og egenandel, for å kunne ta lånet.
def huslånkrav(huslån):
    årslønn = huslån / 5
    egenandel = huslån * 0.15
    return årslønn, egenandel  # Returnere to verdier


print(huslånkrav(1000000))  # (200000, 150000)

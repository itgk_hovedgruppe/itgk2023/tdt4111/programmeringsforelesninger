# Lage en liste
liste = [2, 3, 5, 7, 11]

# Gjenta for hvert tall i listen.
for tall in liste:
    # Oppdatere hvert fall (eller det ser det ut som, men ...)
    tall = 2*tall

print(liste)
# Vi oppdaterer ikke listen. `tall` her er bare en _kopi_ av tallene.

# Gjenta for hver indeks i liste
for index in range(len(liste)):
    # print(liste[index])
    # Her oppdaterer vi faktisk listen, ikke bare en kopi
    liste[index] = 2*liste[index]

print(liste)

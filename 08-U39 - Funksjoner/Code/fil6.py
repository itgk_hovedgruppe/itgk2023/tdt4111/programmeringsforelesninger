# Denne funksjonen tar i mot svaret på et eller annet, og hva vi forventer det
# blir, og sjekker om det stemmer.
def nesten_lik(svar, forventet):
    if forventet - 0.0000001 < svar < forventet + 0.0000001:
        return True
    else:
        return False


svar = 0.1 + 0.2
forventet = 0.3
print(nesten_lik(svar, forventet))

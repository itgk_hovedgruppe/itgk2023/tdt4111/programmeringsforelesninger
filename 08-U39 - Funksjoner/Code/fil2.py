# Oppgave: Rund av alle elementene i A, og lagre i B
A = [1.2, 3.4, 5.6, 7.8, 9.10]
B = [0, 0, 0, 0, 0]

# Denne linjen hadde gjort det samme som linje 3, men automatisk.
# B = [0] * len(A)


# Gjenta for hver indeks i A.
for i in range(len(A)):
    # Sett B på indeksen i lik avrundet A på indeksen i.
    B[i] = round(A[i])

print(B)

# Samme som forrige gang, men med to endringer:
# - kast_terning har en default-verdi for x: 6.
# - En av kjøringene spesifisere hvilke parameter den sender inn.
import random


# x har standardverdi 6. Det vil si at vi ikke _må_ sende inn x som argument.
def kast_terning(x=6):
    return random.randint(1, x)


terningkast = kast_terning(x=20)
print(terningkast)


print(kast_terning())  # 1-6
print(kast_terning(20))  # 1-20
print(kast_terning(x=10))  # 1-10

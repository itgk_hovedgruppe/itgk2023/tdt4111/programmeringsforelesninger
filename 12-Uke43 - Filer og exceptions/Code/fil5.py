# Slik ser en csv-fil ut inni. Komma mellom hver verdi. Nye rader på hver sin
# linje.

# NB!! Det kan hende din Excel liker ";" i stedet for ",". Hvis du får alle
# verdiene i samme kolonne, kan du prøve det. Eller endre på noen
# Excel-instillinger så den støtter komma.

tekst = '''Bokstav,Tall
e,5
f,6
g,7
h,8
'''

# Skriver vi denne dataen til en fil som heter et-eller-annet.csv, så har vi
# basically laget en Excel-fil. :D

with open('data2.csv', 'w') as fil:
    fil.write(tekst)

# Denne funksjonen deler a på b.
def div(a, b):
    return a / b


# Men hva hvis vi deler på 0? Den krasjer.
print(div(12, 0))

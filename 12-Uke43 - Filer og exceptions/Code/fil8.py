'''Paths

Vi rakk ikke mye om det, men "paths" - eller "filsti" på norsk (?) - er et
viktig tema. Filsystemet på PCen din består av en haug med mapper og filer.

Si at du har en mappe "Program". Denne mappen har en undermappe "Data".
Data-mappen har to filer: "data1.csv" og "data2.csv".
I "Program"-mappen har vi i tillegg én fil: "Analyser-data.py".
Mappe-strukturen vil da se noe sånt ut:

Program
|  Data
|  |  - data1.csv
|  |  - data2.csv
|  - Analyser-data.py

I Microsoft Explorer eller Apple sin Finder kan vi se filene og mappene på
vanlig måte. Men hva hvis Analyser-data.py-programmet vårt skal lese fiene som
ligger i Data-mappen? Kan vi bare si noe sånt som:

`with open('data1.csv', ...)'`?

Nei, det kan vi ikke. Python leter _bare_ i
mappen vi jobber i atm. Current Workding Directory (eller CWD). Altså direkte
i Program-mappen. Data-filene ligger i Data-mappen. Vi kan dermed referere til
filene på følgende vis:

`with open('Data/data1.csv', ...)'`

Dette er såkalt "relative path", eller relativ filsti. Altså, hvor må vi
bevege oss i filsystemet fra der vi er (cwd) for å komme til filen vi vil åpne?

Vi har også "absolutt path". Det er nøyaktiv hvis vi eller filen er. Tenk på
det som en slags GPS-koordinater. Dette kommer også an på systemet ditt. For
meg, på min Macbook, ligger akkurat _denne_ filen i følgende absolutte path:

/Users/paul_knutson/src/NTNU/TDT4111/Forelesninger-2023/12-Uke43 - Filer og exceptions/Code.fil8.py

Starter vi fra venstre, kan vi se at vi starter med /. Dette er root-dir, eller
rot-mappen. Alt på hele PCen ligger inni her. Så ikke slett "/". :C

Inni / ligger Users. Her ligger det en mappe per bruker på PCen din.
Sannsynligvis bare brukeren din, og kanskje en administratorbruker du ikke har
tilgang til.

Inni Users ligger paul_knutson, altså brukernavnet ditt på PCen.

Inni der igjen har jeg laget mappe som heter "src" (som står for source kode).

Inni denne igjen har jeg en mappe som heter "NTNU", og inni der igjen en som
heter TDT4111, etterfulgt av en som heter Forelesninger-2023, med enda en mappe
som heter 12-Uke43 - Filer og Exceptions, og til sist en mappe som heter Code.

Inne Code-mappen ligger fil8.py. Denne filen.

Har du Windows starter pathen sannsynligvis på "C:\\" i stedet for "/". Ellers
er systemene nokså like. Windows liker å bruke en back-slash ("\\") i stedet,
men skjønner vanlig "/" også.

Så hvis jeg skal åpne datafilen, kan jeg gjøre noe sånt som dette

with open('Users/paul_knutson/src/.../data1.csv)

(Bare ikke med ...). Men det er kjekkere å bruke relative path.
'''

# Skal vi lagre en fil i mappen 'datafiler', må vi lage filen
# 'datafiler/filnavn'. F.eks. denne, fra fil5.py

tekst = '''Bokstav,Tall
e,5
f,6
g,7
h,8
'''

with open('datafiler/data4.csv', 'w') as fil:
    fil.write(tekst)

# Dette krever at datafiler-mappen finnes, men data4.csv trenger ikke å
# eksistere (siden write-mode lager filen).

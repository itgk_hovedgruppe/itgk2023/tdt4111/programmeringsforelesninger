# Det er to typiske måter å åpne filer på.
# 1. Den ene er med with. Denne er bedre, men ikke alltid like forståelig.
# 2. Den andre er uten with, og da må du huske å kjøre fil.close().

# Eksemplene under gjør nøyaktig det samme.


# Versjon 1
with open('fil.txt', 'r') as fil:
    tekst = fil.read()


# Versjon 1
fil = open('fil.txt', 'r')
tekst = fil.read()
fil.close()



print(tekst)

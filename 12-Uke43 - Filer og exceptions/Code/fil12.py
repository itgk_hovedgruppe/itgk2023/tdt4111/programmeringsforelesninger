# Vi kan også prøve å lese en fil, og ikke bry oss om filen finnes eller om vi
# har lov til å åpne den. Bare prøv. Just try - i en try-blokk! :D
def prøv_å_lese_fil():
    try:
        # Prøv å lese filen.
        with open('fil3.txt', 'r') as fil:
            return fil.read()
    except FileNotFoundError:
        # Hvis det krasjet med en FileNotFoundError: Gjør følgende:
        return 'Fant ikke fil.'
    except PermissionError:
        # Hvis det krasjet med en PermissionError: Gjør følgende:
        return 'Ikke tilgang.'
    except:
        # Hvis det krasjet med en annen feilmelding: Gjør følgende:
        return 'Det krasjet. Vet ikke hvorfor.'


# Lese fil uten at den vil krasje. Kjempebra! :D
print(prøv_å_lese_fil())


# Kjøres denne - uten at fil3.txt finnes - vil den krasje, og den er ikke i en
# try-blokk, så programmet vårt vil krasje og avslutte. Ikke like bra.
with open('fil3.txt', 'r') as fil:
    print( fil.read())

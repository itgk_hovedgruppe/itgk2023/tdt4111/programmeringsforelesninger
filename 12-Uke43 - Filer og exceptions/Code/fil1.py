# Lage en variabel vi skal skrive til fil
tekst = 'Hei sann :D'


# Åpne filen 'fil.txt' i write-mode. (Filen må ikke å eksistere allerede.)
with open('fil.txt', 'w') as fil:
    # Skrive teksten til filen vi har åpnet.
    fil.write(tekst)

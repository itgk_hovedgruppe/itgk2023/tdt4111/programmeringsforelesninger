# Denne funksjonen sjekker først om b == 0. Hvis den er det, returnerer vi
# heller noe annet. F.eks. 0.0. Ikke matematisk riktig, men kanskje bedre enn å
# krasje programmet.
def div(a, b):
    if b == 0:
        return 0.0
    return a / b


# Krasjer ikke programmet ved å "dele på null" :D
print(div(12, 0))

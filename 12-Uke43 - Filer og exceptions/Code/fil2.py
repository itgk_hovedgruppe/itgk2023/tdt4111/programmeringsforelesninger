# Åpne filen 'fil.txt' i read-mode. (Filen må eksistere, ellers krasjer det.)
with open('fil.txt', 'r') as fil:
    # Les filen vi har åpnet inn i tekst-variablen.
    tekst = fil.read()

# Skriv ut teksten.
print(tekst)

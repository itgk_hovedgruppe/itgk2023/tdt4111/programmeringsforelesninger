# Teste om en variabel er et tall.
def is_number(x):
    try:
        # Prøve: Konverter x til et kommatall.
        float(x)

        # Hvis den ikke har krasjet enda: Yay. Det er et tall. Returner True.
        return True
    except:
        # Hvis float(x) krasjet, er x ikke et tall. Returner False.
        return False


# Teste programmet vårt.
print(is_number('3'))  # True
print(is_number('3.14'))  # True
print()
print(is_number('3.14.15'))  # False
print(is_number('3,14'))  # False
print(is_number('A14'))  # False

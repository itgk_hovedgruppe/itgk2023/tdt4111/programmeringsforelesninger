# Men vi kan også bare dele på null og la den krasje, og så deale med feilen
# etterpå.
def div(a, b):
    try:
        # Alt inni try-blokken vil bli kjørt. Hvis den krasjer, hopper vi til
        # except-blokken.
        return a / b  # Hvis b != 0: Fint. Return svaret. Hvis b == 0: except.
    except:
        # Hvis try-blokken krasjer (f.eks. fordi vi delta på 0), kjøres dette.
        return 0.0


# Kan fortsatt dele på null! :D
print(div(12, 0))

# Vi har laget en fil: data.csv. Jeg lagde den i Excel, men du kan lage den i
# hva som helst.
# - I Excel: Lag tabell, lagre som, filtype, csv-fil.
# - Uten Excel: Lag en tekstfil, kall den et-eller-annet.csv (hvis du ser
# fil extensions), og skriv inn samme tekst som i data.csv i denne mappen.


# Åpne og lese filen, på samme måte som en vanlig tekstfil.
with open('data.csv', 'r') as fil:
    tekst = fil.read()


print(tekst)

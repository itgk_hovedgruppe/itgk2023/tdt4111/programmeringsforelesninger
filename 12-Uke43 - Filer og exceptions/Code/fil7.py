'''
Vi lagde et program som skrev en csv-fil fra en hard-kodet string. Hva hvis vi
vil lage den fra en tabell i stedet?

Vi lager en stegliste over hva vi må gjøre.
1. Konverter tabell til string (bygg opp string med tabell-verdier).
2. Bruk kode fra i stad til å lagre stringen i en csv-fil.

csv-filen burde da ha følgende tekst i:
tekst = """Bokstav,Tall
e,5
f,6
g,7
h,8"""

Bytter vi ut linjeskift/newline med \\n, så kan vi se en litt enklere (?)
versjon av teksten vi vet vi må lage.

tekst = "Bokstav,Tall\ne,5\nf,6\ng,7\nh,8"
'''


# Lage en tabell: En liste med rader (som også er lister).
# Altså en liste med lister. List-ception. Memes.

tabell = [  # Bokstav, Tall
    # ['Bokstav', 'Tall'],
    ['e', 5],
    ['f', 6],
    ['g', 7],
    ['h', 8]
]

# Vi kan legge til headers i tabellen - som vist kommentert ut - men vil gjerne
# slippe det. Da må ev. dataanalysekodesnutter ignorere første rad, som er
# stress.


# Vi lager en tom string. Eller, i dette tilfellet legger vi inn en linje med
# headers først.
tekst = 'Bokstav,Tall\n'  # Kol1, kol2 og linjeskift.
for rad in tabell:  # Itererer gjennom en og en rad (liste) i tabellen.
    bokstav = rad[0]  # Deler opp raden i bokstav ...
    tall = rad[1]  # ... og tall.
    # bokstav, tall = rad  # Ev. gjør det på denne måten.
    tekst += f'{bokstav},{tall}\n'  # Lager en f-string (e,3\\n), inn i tekst.


# Lagre string til fil.
with open('data3.csv', 'w') as fil:
    fil.write(tekst)

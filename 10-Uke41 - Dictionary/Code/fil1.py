# Kopiering vs referanser

# Lage variabler
x = 3
y = 4

# _KOPIERE_ tallene
x = y

# Oppdatere den ene kopien
y = 5

print(x, y)

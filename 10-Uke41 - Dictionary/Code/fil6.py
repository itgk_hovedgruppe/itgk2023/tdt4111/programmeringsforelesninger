## Set
# Koden er noe utenfor pensum, men poenget er at den ene er utrolig mye raskere
# enn den andre. Under forelesning brukte lister ca. 2 sekunder, og set ca.
# 0.0004 sekunder.

# Nytteverdi av sett, del 2: `... in sett` er utrolig mye raskere enn
# `... in liste`.


# Importere timeit-modulen, som kan brukes til å se hvor lang tid en kodesnutt
# bruker på å kjøre.
import timeit


# Kjør testen `90001 in liste` 1 000, hvor `liste` er `0, 1, ..., 99999`.
print(timeit.timeit(
    setup = 'liste = list(range(100_000))',
    stmt = '90001 in liste',
    number = 1_000
))

# Kjør testen `90001 in sett` 1 000, hvor `sett` er `0, 1, ..., 99999`.
print(timeit.timeit(
    setup = 'liste = set(list(range(100_000)))',
    stmt = '90001 in liste',
    number = 1_000
))

# Eneste forskjellen er datatypen vi sjekker om inneholder 90001. Forskjellen
# er stor. Bruk sett. :D

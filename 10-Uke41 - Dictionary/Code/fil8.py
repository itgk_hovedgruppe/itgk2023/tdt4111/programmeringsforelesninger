## Dict
# Se Excel/PDF for eksempel på Excel-representasjon av dette.

# Lage dicten.
president = {
    'navn': 'Obama',
    'land': 'USA',
    'alder': 62,
    'barn': ['Per', 'Pål']
}
print(president)

# Oppdatere/endre verdien i feltet `alder` i dicten `president`.
president['alder'] += 1
print(president)

# Skrive ut verdien med indeks, og med get-funksjonen.
print(president['alder'])
print(president.get('partner', 'Ugift'))
# Fordel med `dict.get(felt)`: Hvis `felt` ikke eksisterer, så kan vi be `get`
# om en alternativ verdi. I dette tilfellet `Ugift`. Da slipper vi å først
# teste om `parner` finnes i `president`.

# Bruk dicts!!! :D :D

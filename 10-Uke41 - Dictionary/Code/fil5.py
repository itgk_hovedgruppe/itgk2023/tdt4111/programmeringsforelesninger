## Set

# Mest nyttig del 1: Fjerne duplikater.
liste = [1, 2, 2, 3,3,3 ,4, 4 ,5]

# Lage til set og tilbake til liste.
liste = list(set(liste))
print(liste)


'''
Ps. Set har også flere andre innebyggede funksjoner (metoder), men det kan dere
lese mer om selv. F.eks.

sett.add(3)
sett.remove(3)
'''

president = {
    # key : value
    'navn': 'Obama',
    'land': 'USA',
    'alder': 62,
    'barn': ['Per', 'Pål']
}

# Gå gjennom keys-kolonnen.
for i in president:
    print(i)

# Gå gjennom keys-kolonnen (men mer tydelig).
for i in president.keys():
    print(i)

# Gå gjennom values-kolonnen.
for i in president.values():
    print(i)

# Gå gjennom både keys og values på en gang. Bruk denne masse!
for key, value in president.items():
    print(key, value)

# x, y = f(3)

## Set
# Oppgave: Finn intersection mellom A og B.

# Lage A og B.
A = {'a', 'b', 'c', 'd', 'e'}
B = {'c', 'd', 'e', 'f', 'g'}

# Bruke set.intersection(set)-funksjonen.
C = A.intersection(B)

print(C)  # {'c', 'd', 'e'}

## Set

# To måter å lage set på. 1. Lage liste o.l. og konvertere til set.
A = [1, 2, 3, 4, 5]
A = set(A)

# 2. Lage set direkte.
A = {1, 2, 3, 4, 5}
B = {4, 5, 6, 7, 8}

# Finne hvilke elementer som finnes i A og/eller B.
print(A.union(B))

# Elementer som finnes i _både_ A og B.
print(A.intersection(B))

# Finne elementer som _bare_ finnes i A/B.
print(A.difference(B))
print(B.difference(A))

# Finne elementer som finnes i A _eller_ B, men _ikke_ begge.
print(A.symmetric_difference(B))

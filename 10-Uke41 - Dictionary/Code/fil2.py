# Kopiering vs referanser

# Lage variabler
x = [1, 2, 3]
y = [4, 5, 6]

# _IKKE_ kopiere listene, bare ref. til listene (se vedlagt Excel/PDF)
x = y

# Oppdatere _begge_ listene, fordi de ikke er kopier
y[0] = 7

print(x, y)

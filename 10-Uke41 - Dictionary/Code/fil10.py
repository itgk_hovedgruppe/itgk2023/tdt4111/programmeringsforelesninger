## Dict
# Oppgave: Gjør alle values om til lowercase.

# Modifisert dict, så alt er strings.
president = {
    'navn': 'Obama',
    'land': 'USA',
    'alder': '62',
    'barn': 'Per og Pål'
}


# Gå gjennom ett og ett key-value-par, og oppdater dict[key] til value.lower()
for key, value in president.items():
    president[key] = value.lower()


print(president)

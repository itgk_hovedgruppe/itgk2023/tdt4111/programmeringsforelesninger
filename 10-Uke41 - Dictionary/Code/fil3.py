# Kopiering vs referanser (hvordan å faktisk kopiere lister ++)

# Importere kopi-modul
import copy

# Lage lister
x = [1, 2, 3]
y = [4, 5, 6]

# Tre metoder
x = y[:]  # Slicing: Trenger _ikke_ copy-modulen
x = copy.copy(y)  # copy.copy: Trenger modulen.
x = copy.deepcopy(y)  # copy.deepcopy: Trenger også modulen. Gjør en "dyp" kopi
# Note: deepcopy er viktig hvis du har en liste med lister inni. Ellers blir¨
# bare den ytterste listen kopiert, og resten forblir referanser til de andre
# listene i minnet.

# Oppdatere _kopien_.
y[0] = 7

print(x, y)

## Dict
# Oppgave: Sjekk om `bruker` er registrert i `brukere`.

brukere = {
    'a@gmail.com': 'Paul Knutson',
    'b@gmail.com': 'Erna Solberg',
    'c@gmail.com': 'Barrack Obama',    
}


# bruker = 'a@gmail.com'  # True
bruker = 'd@gmail.com'  # False

print(bruker in brukere)
